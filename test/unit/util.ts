import { strict as assert } from 'assert'
import { ISubjectLedger, Subject } from '../../src/subject'
import { SpanData, TypeSpanData } from '../../src/SpanData'


export class MockSubjectLedger implements ISubjectLedger {
  public glossary = new Map<string, Subject>()
  public activeNames = new Set<string>()
  link() { return }
  unlink() { return }
  byName() { return undefined }
  subjectColor() { return undefined }
  subjectStyle() { return {} }
  isSubjectActive() { return true }
  activateSubject() { return }
  deactivateSubject() { return }
  toggleSubject() { return }
  activateAllSubjects() { return }
  deactivateAllSubjects() { return }
}


export function stringToMark (
  sourceText: string, subStr: string, subjects: string[], startIdx: number = 0
) {
  const begChar = sourceText.indexOf(subStr, startIdx)
  return {
    begChar, endChar: begChar + subStr.length, subjects
  }
}

export function checkSpanData(
  sourceText: string,
  spanData: SpanData,
  typeSpan: TypeSpanData, subStr: string, subjects: string[], startIdx?: number
) {

  let keyPrefix = ''
  let cssClasses = []
  switch(typeSpan) {
  case TypeSpanData.Mark:
    keyPrefix = 'markrange'
    cssClasses = subjects.map(sub => 'mt-subject-' + sub)
    break
  case TypeSpanData.Meta:
    throw Error('Check for Meta in tests using function checkMetaSpanData')
  case TypeSpanData.Selection:
    keyPrefix = 'selection'
    cssClasses = ['mt-selection']
    break
  case TypeSpanData.Text:
    keyPrefix = 'text'
    cssClasses = ['mt-textspan']
    break
  }
  const begChar = sourceText.indexOf(subStr, startIdx)
  const endChar = begChar + subStr.length
  const key = keyPrefix + '-' + begChar + '-' + endChar

  assert.equal(spanData.charRange[0], begChar)
  assert.equal(spanData.charRange[1], endChar)
  assert.equal(spanData.key, key)
  for (let idx = 0; idx < cssClasses.length; ++idx) {
    assert.equal(spanData.cssClasses[idx], cssClasses[idx])
  }
  for (let idx = 0; idx < subjects.length; ++idx) {
    assert.equal(spanData.subjects[idx], subjects[idx])
  }
  assert.equal(spanData.text, sourceText.substring(begChar, endChar))
  // Do not check controller event handlers

}

export function checkMetaSpanData(
  sourceText: string, spanData: SpanData, charIdx: number, text: string
) {

  const keyPrefix = 'meta-char-'
  const cssClasses = ['mt-meta-char']
  const begChar = charIdx
  const endChar = charIdx
  const key = keyPrefix + text + '-' + charIdx + '-' + charIdx

  assert.equal(spanData.charRange[0], begChar)
  assert.equal(spanData.charRange[1], endChar)
  assert.equal(spanData.key, key)
  for (let idx = 0; idx < cssClasses.length; ++idx) {
    assert.equal(spanData.cssClasses[idx], cssClasses[idx])
  }
  assert.equal(spanData.subjects.length, 0)
  assert.equal(spanData.text, text)
  // Do not check controller event handlers

}
