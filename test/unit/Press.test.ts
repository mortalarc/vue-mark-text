import { strict as assert } from 'assert'
import 'mocha'

import { Layers } from '../../src/manuscript/Layer'
import { IMark } from '../../src/IMark'
import { SpanData, TypeSpanData } from '../../src/SpanData'
import { Press } from '../../src/manuscript/Press'
import { checkSpanData, stringToMark } from './util'


describe('Press', function() {

  const mockWindow = {
    setTimeout(callback: () => void, ...args: any[]) {
      return global.setTimeout(callback, ...args) as unknown as number
    }
  }
  const windowRef = global.window
  beforeEach(async () => {
    global.window = mockWindow as unknown as Window & typeof globalThis
  })
  afterEach(async () => {
    global.window = windowRef
  })

  const sourceText = 'zero one two three four five six seven eight nine'
  const numToMark = (num: string, subs: string[]) => stringToMark(sourceText, num, subs)

  describe('SpanData', function() {

    const checkCharRange = (
      spanData: SpanData,
      typeSpan: TypeSpanData, num: string, subjects: string[], actualBegChar?: number
    ) => checkSpanData(sourceText, spanData, typeSpan, num, subjects, actualBegChar)

    it('produces SpanData directly from a single layer with overlaps', async function() {

      const layerMarks = [
        numToMark('zero', []),
        numToMark('two', ['sub1']),
        numToMark('three four five', ['sub2']),
        numToMark('five six seven', ['sub3']),
        numToMark('eight', ['sub2', 'sub4']),
      ]
      const layerInit: [string, TypeSpanData, IMark[]][] = [
        ['layer', TypeSpanData.Mark, layerMarks],
        ['layerText', TypeSpanData.Text, [stringToMark(sourceText, sourceText, [])]]
      ]

      const layers = new Layers(layerInit)
      const spanBatches: SpanData[][] = []
      const press = new Press(sourceText, spanBatches, layers, 20)
      await press.waitForReady()

      assert.strictEqual(spanBatches.length, 1)
      const spans = spanBatches[0]
      assert.strictEqual(spans.length, 10)
      checkCharRange(spans[0], TypeSpanData.Mark, 'zero', [])
      checkCharRange(spans[1], TypeSpanData.Text, ' one ', [])
      checkCharRange(spans[2], TypeSpanData.Mark, 'two', ['sub1'])
      checkCharRange(spans[3], TypeSpanData.Text, ' ', [], 12)
      checkCharRange(spans[4], TypeSpanData.Mark, 'three four ', ['sub2'])
      checkCharRange(spans[5], TypeSpanData.Mark, 'five', ['sub2', 'sub3'])
      checkCharRange(spans[6], TypeSpanData.Mark, ' six seven', ['sub3'])
      checkCharRange(spans[7], TypeSpanData.Text, ' ', [], 38)
      checkCharRange(spans[8], TypeSpanData.Mark, 'eight', ['sub2', 'sub4'])
      checkCharRange(spans[9], TypeSpanData.Text, ' nine', [])

    })

    it('produces SpanData from multiple overlapping layers by priority', async function() {

      const layer0Marks = [ numToMark('three four five', ['sub0']) ]
      const layer1Marks = [ numToMark('four five six', ['sub1']) ]
      const layer2Marks = [ numToMark('five six seven', ['sub2']) ]
      const layerInit: [string, TypeSpanData, IMark[]][] = [
        ['layer0', TypeSpanData.Mark, layer0Marks],
        ['layer1', TypeSpanData.Mark, layer1Marks],
        ['layer2', TypeSpanData.Mark, layer2Marks],
        ['layerText', TypeSpanData.Text, [stringToMark(sourceText, sourceText, [])]]
      ]

      const layers = new Layers(layerInit)
      const spanBatches: SpanData[][] = []
      const press = new Press(sourceText, spanBatches, layers, 20)
      await press.waitForReady()

      assert.strictEqual(spanBatches.length, 1)
      let spans = spanBatches[0]
      assert.strictEqual(spans.length, 5)
      checkCharRange(spans[0], TypeSpanData.Text, 'zero one two ', [])
      checkCharRange(spans[1], TypeSpanData.Mark, 'three four five', ['sub0'])
      checkCharRange(spans[2], TypeSpanData.Mark, ' six', ['sub1'])
      checkCharRange(spans[3], TypeSpanData.Mark, ' seven', ['sub2'])
      checkCharRange(spans[4], TypeSpanData.Text, ' eight nine', [])

      layers.permutePriority([2, 1, 0, 3])
      press.reset()
      await press.waitForReady()

      assert.strictEqual(spanBatches.length, 1)
      spans = spanBatches[0]
      assert.strictEqual(spans.length, 5)
      checkCharRange(spans[0], TypeSpanData.Text, 'zero one two ', [])
      checkCharRange(spans[1], TypeSpanData.Mark, 'three ', ['sub0'])
      checkCharRange(spans[2], TypeSpanData.Mark, 'four ', ['sub1'])
      checkCharRange(spans[3], TypeSpanData.Mark, 'five six seven', ['sub2'])
      checkCharRange(spans[4], TypeSpanData.Text, ' eight nine', [])

      layers.permutePriority([1, 0, 2, 3])
      press.reset()
      await press.waitForReady()

      assert.strictEqual(spanBatches.length, 1)
      spans = spanBatches[0]
      assert.strictEqual(spans.length, 5)
      checkCharRange(spans[0], TypeSpanData.Text, 'zero one two ', [])
      checkCharRange(spans[1], TypeSpanData.Mark, 'three ', ['sub0'])
      checkCharRange(spans[2], TypeSpanData.Mark, 'four five six', ['sub1'])
      checkCharRange(spans[3], TypeSpanData.Mark, ' seven', ['sub2'])
      checkCharRange(spans[4], TypeSpanData.Text, ' eight nine', [])

    })

    it('can calculate a patch of SpanData from changes', async function() {

      const layer0Marks = [ numToMark('three four five', ['sub0']) ]
      const layer1Marks = [ numToMark('four five six', ['sub1']) ]
      const layer2Marks = [ numToMark('five six seven', ['sub2']) ]
      const layerInit: [string, TypeSpanData, IMark[]][] = [
        ['layer0', TypeSpanData.Mark, layer0Marks],
        ['layer1', TypeSpanData.Mark, layer1Marks],
        ['layer2', TypeSpanData.Mark, layer2Marks],
        ['layerText', TypeSpanData.Text, [stringToMark(sourceText, sourceText, [])]],
      ]

      let layers = new Layers(layerInit)
      let spanBatches: SpanData[][] = []
      let press = new Press(sourceText, spanBatches, layers, 20)

      layers.addToLayer('layer0', numToMark('one', ['sub3']))
      layers.rmFromLayer('layer2', numToMark('five six seven', ['sub2']))
      press.implementPatch()
      await press.waitForReady()

      assert.strictEqual(spanBatches.length, 1)
      let spans = spanBatches[0]
      assert.strictEqual(spans.length, 6)
      checkCharRange(spans[0], TypeSpanData.Text, 'zero ', [])
      checkCharRange(spans[1], TypeSpanData.Mark, 'one', ['sub3'])
      checkCharRange(spans[2], TypeSpanData.Text, ' two ', [])
      checkCharRange(spans[3], TypeSpanData.Mark, 'three four five', ['sub0'])
      checkCharRange(spans[4], TypeSpanData.Mark, ' six', ['sub1'])
      checkCharRange(spans[5], TypeSpanData.Text, ' seven eight nine', [])

      layers = new Layers(layerInit)
      spanBatches = []
      press = new Press(sourceText, spanBatches, layers, 20)

      layers.addToLayer('layer0', numToMark('one', ['sub3']))
      layers.addToLayer('layer2', numToMark('eight', ['sub4']))
      press.implementPatch()
      await press.waitForReady()

      assert.strictEqual(spanBatches.length, 1)
      spans = spanBatches[0]
      assert.strictEqual(spans.length, 9)
      checkCharRange(spans[0], TypeSpanData.Text, 'zero ', [])
      checkCharRange(spans[1], TypeSpanData.Mark, 'one', ['sub3'])
      checkCharRange(spans[2], TypeSpanData.Text, ' two ', [])
      checkCharRange(spans[3], TypeSpanData.Mark, 'three four five', ['sub0'])
      checkCharRange(spans[4], TypeSpanData.Mark, ' six', ['sub1'])
      checkCharRange(spans[5], TypeSpanData.Mark, ' seven', ['sub2'])
      checkCharRange(spans[6], TypeSpanData.Text, ' ', [], 38)
      checkCharRange(spans[7], TypeSpanData.Mark, 'eight', ['sub4'])
      checkCharRange(spans[8], TypeSpanData.Text, ' nine', [])

    })

  })

})
