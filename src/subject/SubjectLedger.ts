import { ISubjectComposable } from '@/IMarkedText'
import { ISubject, Subject } from './Subject'


export interface ISubjectLedger {
  get glossary(): Map<string, Subject>
  get listSubjects(): ISubject[]
  get listActiveSubjects(): ISubject[]
  link(composable: ISubjectComposable): void
  unlink(): void
  byName(name: string): Subject | undefined
  subjectColor(subjectName: string): string | undefined
  subjectStyle(subjectName: string): any
  isSubjectActive(subjectName: string): boolean
  activateSubject(subjectName: string): void
  deactivateSubject(subjectName: string): void
  toggleSubject(subjectName: string): void
  activateAllSubjects(): void
  deactivateAllSubjects(): void
}


export class SubjectLedger implements ISubjectLedger {

  private composable: ISubjectComposable | null
  private xGlossary: Map<string, Subject>

  public constructor() {
    this.composable = null
    this.xGlossary = new Map()
  }


  public get glossary() {
    return this.xGlossary
  }
  public get listSubjects() {
    return [...this.glossary.values()]
  }
  public get listActiveSubjects() {
    return this.listSubjects.filter(sub => sub.active)
  }


  public link(composable: ISubjectComposable) {
    this.composable = composable
  }
  public unlink() {
    this.composable = null
  }


  public byName(name: string) {
    return this.glossary.get(name)
  }

  public subjectColor(subjectName: string) {
    return this.glossary.get(subjectName)?.color
  }
  public subjectStyle(subjectName: string) {
    return this.glossary.get(subjectName)?.style
  }

  public isSubjectActive(subjectName: string): boolean {
    const subject = this.glossary.get(subjectName)
    return (subject != null && subject.active)
  }
  public activateSubject(subjectName: string) {
    const subject = this.glossary.get(subjectName)
    if (subject != null && !subject.active) {
      subject.active = true
      this.composable?.updateMarks()
    }
  }
  public deactivateSubject(subjectName: string) {
    const subject = this.glossary.get(subjectName)
    if (subject != null && subject.active) {
      subject.active = false
      this.composable?.updateMarks()
    }
  }
  public toggleSubject(subjectName: string) {
    const subject = this.glossary.get(subjectName)
    if (subject != null) {
      if (subject.active) this.deactivateSubject(subjectName)
      else this.activateSubject(subjectName)
    }
  }
  public activateAllSubjects() {
    this.glossary.forEach(subject => {
      subject.active = true
    })
  }
  public deactivateAllSubjects() {
    this.glossary.forEach(subject => { subject.active = false })
  }

}
