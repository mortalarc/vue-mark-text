import { strict as assert } from 'assert'
import 'mocha'
import { BubbleBatchArray } from 'etali'

import { mergePatches, Patch, PatchType } from '../../src/manuscript/Patch'


describe('Patch', function() {

  it('can tell if it is empty', function() {

    const patch = new Patch()
    assert(patch.empty)
    patch.added = [[0, 'zero']]
    assert(!patch.empty)
    patch.rmed = [[1, 'one']]
    assert(!patch.empty)
    patch.added = []
    assert(!patch.empty)
    patch.rmed = []
    assert(patch.empty)

  })

  it('transforms a basic array as expected', function() {

    const arr = [0, 1, 2, 3, 4]
    const patch = new Patch([[0, -1], [2, 1.5], [5, 4.5]], [[1, 1], [2, 2]])
    patch.transform(arr)
    assert.deepEqual(arr, [-1, 0, 1.5, 3, 4, 4.5])

  })

  it('transforms a bubble batch array as expected', function() {

    const arr = BubbleBatchArray([0, 1, 2, 3, 4], 3, 1)
    assert.deepEqual(arr.rawBatches, [[0, 1], [2, 3], [4]])
    const patch = new Patch([[0, -1], [2, 1.5], [5, 4.5]], [[1, 1], [2, 2]])
    patch.transform(arr)
    assert.deepEqual(arr.rawBatches, [[-1, 0, 1.5], [3], [4, 4.5]])

  })

  it('can be merged with other patches', function() {

    const patch0 = new Patch([[0, 'a'], [2, 'b']], [[1, 'c']])
    const patch1 = new Patch([[0, 'd']], [[3, 'b']])
    const comboPatch = mergePatches(patch0, patch1)
    assert.deepEqual(comboPatch.added, [[0, 'd'], [0, 'a'], [2, 'b']])
    assert.deepEqual(comboPatch.rmed, [[1, 'c'], [2, 'b']])

  })

  it('iterates in a particular order', function() {

    const patch = new Patch([[0, -1], [2, 1.5], [5, 4.5]], [[1, 1], [2, 2]])

    let iter = patch.rmThenAddOrder()
    assert.deepEqual(iter.next().value, [PatchType.add, [0, -1]])
    assert.deepEqual(iter.next().value, [PatchType.rm, [1, 1]])
    assert.deepEqual(iter.next().value, [PatchType.rm, [2, 2]])
    assert.deepEqual(iter.next().value, [PatchType.add, [2, 1.5]])
    assert.deepEqual(iter.next().value, [PatchType.add, [5, 4.5]])
    assert(iter.next().done)

    iter = patch.addThenRmOrder()
    assert.deepEqual(iter.next().value, [PatchType.add, [0, -1]])
    assert.deepEqual(iter.next().value, [PatchType.rm, [1, 1]])
    assert.deepEqual(iter.next().value, [PatchType.add, [2, 1.5]])
    assert.deepEqual(iter.next().value, [PatchType.rm, [2, 2]])
    assert.deepEqual(iter.next().value, [PatchType.add, [5, 4.5]])
    assert(iter.next().done)

  })

  it('applies patches merged and in sequence to produce the same', function() {

    const arr = [0, 1, 2, 3, 4]
    const patch0 = new Patch([[0, -1], [2, 1.5], [5, 4.5]], [[1, 1], [2, 2]])
    const patch1 = new Patch([[0, -1.5], [2, 1.25]], [[3, 3]])

    const mergedPatchArr = [...arr]
    const mergedPatch = patch0.merge(patch1)
    mergedPatch.transform(mergedPatchArr)
    assert.deepEqual(mergedPatchArr, [-1.5, -1, 0, 1.25, 1.5, 4, 4.5])

    const sequentialPatchArr = [...arr]
    patch0.transform(sequentialPatchArr)
    const mappedPatch1 = patch1.mapIndicesFromBasePatch(patch0)
    mappedPatch1.transform(sequentialPatchArr)
    assert.deepEqual(sequentialPatchArr, [-1.5, -1, 0, 1.25, 1.5, 4, 4.5])
    assert.deepEqual(mergedPatchArr, sequentialPatchArr)

  })

  it('maps patch coordinates associatively', function() {

    const arr = [0, 1, 2, 3, 4]
    const patch0 = new Patch([[0, -1], [2, 1.5], [5, 4.5]], [[1, 1], [2, 2]])
    const patch1 = new Patch([[0, -1.5], [2, 1.25]], [[3, 3]])
    const patch2 = new Patch([[1, 0.5]], [[4, 4]])

    // This merging is NOT working correctly.
    const mappedPatch01 = patch1.mapIndicesFromBasePatch(patch0)
    const mappedPatch02 = patch2.mapIndicesFromBasePatch(patch0)
    const mappedPatch12 = patch2.mapIndicesFromBasePatch(patch1)

    const mappedPatch0_12 = patch2.mapIndicesFromBasePatch(patch0).mapIndicesFromBasePatch(patch1)
    const mergedPatch01 = mergePatches(patch0, patch1)
    const mappedPatch01_2 = patch2.mapIndicesFromBasePatch(mergedPatch01)

    const arr0 = patch0.transform([...arr])
    const arr1 = patch1.transform([...arr])
    const arr2 = patch2.transform([...arr])
    assert.deepEqual(arr0, [-1, 0, 1.5, 3, 4, 4.5])
    assert.deepEqual(arr1, [-1.5, 0, 1, 1.25, 2, 4])
    assert.deepEqual(arr2, [0, 0.5, 1, 2, 3])

    const arr01 = mappedPatch01.transform([...arr0])
    const arr02 = mappedPatch02.transform([...arr0])
    const arr12 = mappedPatch12.transform([...arr1])
    assert.deepEqual(arr01, [-1.5, -1, 0, 1.25, 1.5, 4, 4.5])
    assert.deepEqual(arr02, [-1, 0, 0.5, 1.5, 3, 4.5])
    assert.deepEqual(arr12, [-1.5, 0, 0.5, 1, 1.25, 2])

    const arr0_12 = mappedPatch0_12.transform([...arr01])
    const arr01_2 = mappedPatch01_2.transform([...arr01])
    assert.deepEqual(arr0_12, arr01_2)
    assert.deepEqual(arr0_12, [-1.5, -1, 0, 0.5, 1.25, 1.5, 4.5])
    assert.deepEqual(arr01_2, [-1.5, -1, 0, 0.5, 1.25, 1.5, 4.5])

  })

})
