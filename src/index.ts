export { MarkedText } from './component'
export { type IMarkedText } from './IMarkedText'
export { type IColorModule, ManualColorModule, ColorScaleModule } from './ColorModule'
export {
  type IController, AController,
  NullController,
  TextSelectionController,
  WordSelectionController,
  TwoClickController
} from './Controller'
export { type IMark, compareMarks } from './IMark'
export {
  type IManuscript,
  Layers, Manuscript, Patch, Press
} from './manuscript'
export { SpanData, TypeSpanData, createSpanData } from './SpanData'
export {
  type ISubject, type ISubjectLedger, type ISubjectEngine, type SubjectStyleType,
  Subject, SubjectEngine, SubjectLedger
} from './subject'
