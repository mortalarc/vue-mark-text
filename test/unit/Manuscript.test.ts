import { strict as assert } from 'assert'
import 'mocha'
import { fake } from 'sinon'

import { Manuscript } from '../../src/manuscript/Manuscript'
import { SpanData, TypeSpanData } from '../../src/SpanData'
import { checkMetaSpanData, checkSpanData, stringToMark } from './util'


describe('Manuscript', function() {

  const mockRemoveAllRanges = fake()
  const mockSelection = {
    removeAllRanges: mockRemoveAllRanges
  }
  const mockWindow = {
    getSelection() { return mockSelection },
    setTimeout(callback: () => void, ...args: any[]) {
      return global.setTimeout(callback, ...args) as unknown as number
    }
  }
  const windowRef = global.window
  beforeEach(async () => {
    global.window = mockWindow as unknown as Window & typeof globalThis
  })
  afterEach(async () => {
    global.window = windowRef
  })

  const sourceText = 'zero one two three four five six seven eight nine'
  const numToMark = (num: string, subs: string[]) => stringToMark(sourceText, num, subs)
  const checkCharRange = (
    spanData: SpanData,
    typeSpan: TypeSpanData, num: string, subjects: string[], actualBegChar?: number
  ) => checkSpanData(sourceText, spanData, typeSpan, num, subjects, actualBegChar)


  describe('simple marks', () => {

    const marks = [
      numToMark('zero', []),
      numToMark('two', ['sub1']),
      numToMark('four', ['sub2']),
      numToMark('six', ['sub3']),
      numToMark('eight', ['sub2', 'sub4']),
    ]
    const batchSize = 4

    const checkDefaultSpanData = (spanBatches: SpanData[][]) => {
      assert.strictEqual(spanBatches.length, 4)
      const spans0 = spanBatches[0]
      assert.strictEqual(spans0.length, 3)
      checkCharRange(spans0[0], TypeSpanData.Mark, 'zero', [])
      checkCharRange(spans0[1], TypeSpanData.Text, ' one ', [])
      checkCharRange(spans0[2], TypeSpanData.Mark, 'two', ['sub1'])
      const spans1 = spanBatches[1]
      assert.strictEqual(spans1.length, 3)
      checkCharRange(spans1[0], TypeSpanData.Text, ' three ', [])
      checkCharRange(spans1[1], TypeSpanData.Mark, 'four', ['sub2'])
      checkCharRange(spans1[2], TypeSpanData.Text, ' five ', [])
      const spans2 = spanBatches[2]
      assert.strictEqual(spans2.length, 3)
      checkCharRange(spans2[0], TypeSpanData.Mark, 'six', ['sub3'])
      checkCharRange(spans2[1], TypeSpanData.Text, ' seven ', [])
      checkCharRange(spans2[2], TypeSpanData.Mark, 'eight', ['sub2', 'sub4'])
      const spans3 = spanBatches[3]
      assert.strictEqual(spans3.length, 1)
      checkCharRange(spans3[0], TypeSpanData.Text, ' nine', [])
    }


    it('initializes as expected', async function() {

      const manuscript = new Manuscript(sourceText, marks, () => null, batchSize)
      manuscript.refresh()
      await manuscript.waitForReady()

      assert.strictEqual(manuscript.baseText, sourceText)
      assert.strictEqual(manuscript.batchSize, 4)

      checkDefaultSpanData(manuscript.spanBatches)

    })

    it('responds to changing marks', async function() {

      const manuscript = new Manuscript(sourceText, marks, () => null, batchSize)
      manuscript.refresh()
      await manuscript.waitForReady()

      manuscript.changeMarks([
        numToMark('one', []),
        numToMark('three', ['sub1']),
        numToMark('five', ['sub2']),
        numToMark('six', ['sub3']),
        numToMark('eight', ['sub2', 'sub4']),
      ])
      manuscript.refresh()
      await manuscript.waitForReady()

      assert.strictEqual(manuscript.spanBatches.length, 5)
      const spans0 = manuscript.spanBatches[0]
      assert.strictEqual(spans0.length, 3)
      checkCharRange(spans0[0], TypeSpanData.Text, 'zero ', [])
      checkCharRange(spans0[1], TypeSpanData.Mark, 'one', [])
      checkCharRange(spans0[2], TypeSpanData.Text, ' two ', [])
      const spans1 = manuscript.spanBatches[1]
      assert.strictEqual(spans1.length, 2)
      checkCharRange(spans1[0], TypeSpanData.Mark, 'three', ['sub1'])
      checkCharRange(spans1[1], TypeSpanData.Text, ' four ', [])
      const spans2 = manuscript.spanBatches[2]
      assert.strictEqual(spans2.length, 3)
      checkCharRange(spans2[0], TypeSpanData.Mark, 'five', ['sub2'])
      checkCharRange(spans2[1], TypeSpanData.Text, ' ', [], 28)
      checkCharRange(spans2[2], TypeSpanData.Mark, 'six', ['sub3'])
      const spans3 = manuscript.spanBatches[3]
      assert.strictEqual(spans3.length, 2)
      checkCharRange(spans3[0], TypeSpanData.Text, ' seven ', [])
      checkCharRange(spans3[1], TypeSpanData.Mark, 'eight', ['sub2', 'sub4'])
      const spans4 = manuscript.spanBatches[4]
      assert.strictEqual(spans4.length, 1)
      checkCharRange(spans4[0], TypeSpanData.Text, ' nine', [])

    })

    it('can batch changes to SpanData', async function() {

      const manuscript = new Manuscript(sourceText, marks, () => null, batchSize)

      manuscript.changeMarks([
        numToMark('one', []),
        numToMark('three', ['sub1']),
        numToMark('five', ['sub2']),
        numToMark('six', ['sub3']),
        numToMark('eight', ['sub2', 'sub4']),
      ])
      await manuscript.waitForReady()

      checkDefaultSpanData(manuscript.spanBatches)

      manuscript.changeMarks([
        numToMark('one', []),
        numToMark('three four', ['sub1']),
        numToMark('five', ['sub2']),
        numToMark('six', ['sub3']),
        numToMark('eight', ['sub2', 'sub4']),
      ])
      manuscript.refresh()
      await manuscript.waitForReady()

      assert.strictEqual(manuscript.spanBatches.length, 5)
      const spans0 = manuscript.spanBatches[0]
      assert.strictEqual(spans0.length, 3)
      checkCharRange(spans0[0], TypeSpanData.Text, 'zero ', [])
      checkCharRange(spans0[1], TypeSpanData.Mark, 'one', [])
      checkCharRange(spans0[2], TypeSpanData.Text, ' two ', [])
      const spans1 = manuscript.spanBatches[1]
      assert.strictEqual(spans1.length, 2)
      checkCharRange(spans1[0], TypeSpanData.Mark, 'three four', ['sub1'])
      checkCharRange(spans1[1], TypeSpanData.Text, ' ', [], 23)
      const spans2 = manuscript.spanBatches[2]
      assert.strictEqual(spans2.length, 3)
      checkCharRange(spans2[0], TypeSpanData.Mark, 'five', ['sub2'])
      checkCharRange(spans2[1], TypeSpanData.Text, ' ', [], 28)
      checkCharRange(spans2[2], TypeSpanData.Mark, 'six', ['sub3'])
      const spans3 = manuscript.spanBatches[3]
      assert.strictEqual(spans3.length, 2)
      checkCharRange(spans3[0], TypeSpanData.Text, ' seven ', [])
      checkCharRange(spans3[1], TypeSpanData.Mark, 'eight', ['sub2', 'sub4'])
      const spans4 = manuscript.spanBatches[4]
      assert.strictEqual(spans4.length, 1)
      checkCharRange(spans4[0], TypeSpanData.Text, ' nine', [])

    })

    it('manages and prioritizes selection', async function() {

      const manuscript = new Manuscript(sourceText, marks, () => null, batchSize)
      manuscript.refresh()
      await manuscript.waitForReady()

      assert.equal(manuscript.selection, null)

      const selBeg = sourceText.indexOf('three four')
      manuscript.selection = [selBeg, selBeg + 'three four'.length]
      manuscript.refresh()
      await manuscript.waitForReady()

      assert(mockRemoveAllRanges.called)

      assert.deepEqual(manuscript.selection, [13, 23])

      assert.strictEqual(manuscript.spanBatches.length, 4)
      const spans0 = manuscript.spanBatches[0]
      assert.strictEqual(spans0.length, 4)
      checkCharRange(spans0[0], TypeSpanData.Mark, 'zero', [])
      checkCharRange(spans0[1], TypeSpanData.Text, ' one ', [])
      checkCharRange(spans0[2], TypeSpanData.Mark, 'two', ['sub1'])
      checkCharRange(spans0[3], TypeSpanData.Text, ' ', [], 12)
      const spans1 = manuscript.spanBatches[1]
      assert.strictEqual(spans1.length, 1)
      checkCharRange(spans1[0], TypeSpanData.Selection, 'three four', [])
      const spans2 = manuscript.spanBatches[2]
      assert.strictEqual(spans2.length, 4)
      checkCharRange(spans2[0], TypeSpanData.Text, ' five ', [])
      checkCharRange(spans2[1], TypeSpanData.Mark, 'six', ['sub3'])
      checkCharRange(spans2[2], TypeSpanData.Text, ' seven ', [])
      checkCharRange(spans2[3], TypeSpanData.Mark, 'eight', ['sub2', 'sub4'])
      const spans3 = manuscript.spanBatches[3]
      assert.strictEqual(spans3.length, 1)
      checkCharRange(spans3[0], TypeSpanData.Text, ' nine', [])

    })

    it('can set and unset meta strings', async function() {

      const manuscript = new Manuscript(sourceText, marks, () => null, batchSize)
      manuscript.refresh()
      await manuscript.waitForReady()

      const metaStrings: [string, number][] = [
        ['IN THE MIDDLE OF ONE', sourceText.indexOf('ne two')],
        ['IN THE MIDDLE OF SIX', sourceText.indexOf('ix seven')],
        ['RIGHT AFTER SIX', sourceText.indexOf(' seven')]
      ]
      manuscript.setMetaStrings(metaStrings)
      manuscript.refresh()
      await manuscript.waitForReady()

      assert.strictEqual(manuscript.spanBatches.length, 6)
      const spans0 = manuscript.spanBatches[0]
      assert.strictEqual(spans0.length, 3)
      checkCharRange(spans0[0], TypeSpanData.Mark, 'zero', [])
      checkCharRange(spans0[1], TypeSpanData.Text, ' o', [])
      checkMetaSpanData(sourceText, spans0[2], metaStrings[0][1], metaStrings[0][0])
      const spans1 = manuscript.spanBatches[1]
      assert.strictEqual(spans1.length, 2)
      checkCharRange(spans1[0], TypeSpanData.Text, 'ne ', [])
      checkCharRange(spans1[1], TypeSpanData.Mark, 'two', ['sub1'])
      const spans2 = manuscript.spanBatches[2]
      assert.strictEqual(spans2.length, 3)
      checkCharRange(spans2[0], TypeSpanData.Text, ' three ', [])
      checkCharRange(spans2[1], TypeSpanData.Mark, 'four', ['sub2'])
      checkCharRange(spans2[2], TypeSpanData.Text, ' five ', [])
      const spans3 = manuscript.spanBatches[3]
      assert.strictEqual(spans3.length, 3)
      checkCharRange(spans3[0], TypeSpanData.Mark, 's', ['sub3'])
      checkMetaSpanData(sourceText, spans3[1], metaStrings[1][1], metaStrings[1][0])
      checkCharRange(spans3[2], TypeSpanData.Mark, 'ix', ['sub3'])
      const spans4 = manuscript.spanBatches[4]
      assert.strictEqual(spans4.length, 3)
      checkMetaSpanData(sourceText, spans4[0], metaStrings[2][1], metaStrings[2][0])
      checkCharRange(spans4[1], TypeSpanData.Text, ' seven ', [])
      checkCharRange(spans4[2], TypeSpanData.Mark, 'eight', ['sub2', 'sub4'])
      const spans5 = manuscript.spanBatches[5]
      assert.strictEqual(spans5.length, 1)
      checkCharRange(spans5[0], TypeSpanData.Text, ' nine', [])

    })

  })

  describe('complex marks', () => {

    it('fully overlapping marks', async function() {

      const marks = [
        numToMark('zero one', ['sub']),
        numToMark('one', ['sub'])
      ]
      const batchSize = 10
  
      const manuscript = new Manuscript(sourceText, marks, () => null, batchSize)
      manuscript.refresh()
      await manuscript.waitForReady()

      const spanBatches = manuscript.spanBatches
      assert.strictEqual(spanBatches.length, 1)
      const spans0 = spanBatches[0]
      assert.strictEqual(spans0.length, 3)
      checkCharRange(spans0[0], TypeSpanData.Mark, 'zero ', ['sub'])
      checkCharRange(spans0[1], TypeSpanData.Mark, 'one', ['sub'])
      checkCharRange(spans0[2], TypeSpanData.Text, ' two three four five six seven eight nine', [])

    })

  })

})
