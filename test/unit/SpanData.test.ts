import { strict as assert } from 'assert'
import 'mocha'

import { SpanData, createSpanData, TypeSpanData } from '../../src/SpanData'


describe('SpanData', function() {

  const charRange = [0, 9] as [number, number]
  const key = 'key'
  const cssClasses = ['class1', 'class2']
  const subjects = ['subject1', 'subject2']
  const spanControllerEvents = {}
  const text = 'some text'

  it('should initialize its properties', function() {
    const spanData = new SpanData(
      charRange, key, cssClasses, subjects, spanControllerEvents, text
    )
    assert.strictEqual(spanData.charRange, charRange)
    assert.strictEqual(spanData.key, key)
    assert.strictEqual(spanData.cssClasses, cssClasses)
    assert.strictEqual(spanData.subjects, subjects)
    assert.strictEqual(spanData.spanControllerEvents, spanControllerEvents)
    assert.strictEqual(spanData.text, text)
  })

  it('should clone as expected', function() {
    const spanData = new SpanData(
      charRange, key, cssClasses, subjects, spanControllerEvents, text
    )
    const spanData2 = spanData.clone()
    assert.notStrictEqual(spanData.charRange, spanData2.charRange)
    assert.deepEqual(spanData.charRange, spanData2.charRange)
    assert.strictEqual(spanData.key, key)
    assert.strictEqual(spanData.cssClasses, cssClasses)
    assert.strictEqual(spanData.subjects, subjects)
    assert.strictEqual(spanData.spanControllerEvents, spanControllerEvents)
    assert.strictEqual(spanData.text, text)
  })

  it('can be created with factory', function() {

    const textSpanData = createSpanData(TypeSpanData.Mark, { charRange, spanText: text })
    assert.deepEqual(textSpanData.charRange, charRange)
    assert.deepEqual(textSpanData.key, 'text-0-9')
    assert.deepEqual(textSpanData.cssClasses, [ 'mt-textspan' ])
    assert.deepEqual(textSpanData.subjects, [])
    assert.deepEqual(textSpanData.text, text)

  })

})
