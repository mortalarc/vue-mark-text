import { strict as assert } from 'assert'
import 'mocha'

import { Layers } from '../../src/manuscript/Layer'
import { IMark, compareMarks } from '../../src/IMark'
import { TypeSpanData } from '../../src/SpanData'
import { stringToMark } from './util'


describe('Layer', function() {

  const sourceText = 'zero one two three four five six seven eight nine'
  const numToMark = (num: string, subs: string[]) => stringToMark(sourceText, num, subs)

  describe('basic functionality', function() {

    const layer0Marks = [
      numToMark('zero', []),
      numToMark('two', ['sub1']),
      numToMark('four', ['sub2']),
      numToMark('six', ['sub3']),
      numToMark('eight', ['sub2', 'sub4']),
    ]
    const layer1Marks = [
      numToMark('two three four five', [])
    ]
    const layerInit: [string, TypeSpanData, IMark[]][] = [
      ['layer0', TypeSpanData.Mark, layer0Marks],
      ['layer1', TypeSpanData.Selection, layer1Marks],
      ['layerText', TypeSpanData.Text, [stringToMark(sourceText, sourceText, [])]]
    ]

    it('should organize layers correctly', function() {

      const layers = new Layers(layerInit)

      assert.strictEqual(layers.length, 3)
      const layer0 = layers.getLayers()['layer0']
      assert.notStrictEqual(layer0, undefined)
      assert.strictEqual(layer0.typeSpanData, TypeSpanData.Mark)
      assert.deepEqual(layer0.marks, layer0Marks)
      const layer1 = layers.getLayers()['layer1']
      assert.notStrictEqual(layer1, undefined)
      assert.strictEqual(layer1.typeSpanData, TypeSpanData.Selection)
      assert.deepEqual(layer1.marks, layer1Marks)
      const layerText = layers.getLayers()['layerText']
      assert.notStrictEqual(layerText, undefined)
      assert.strictEqual(layerText.typeSpanData, TypeSpanData.Text)
      assert.strictEqual(layers.getLayers()['layer2'], undefined)

      assert.strictEqual(layers.layerByPriority(0), layer0)
      assert.strictEqual(layers.layerByPriority(1), layer1)
      assert.strictEqual(layers.layerByPriority(2), layerText)
      assert.throws(() => layers.layerByPriority(3), /^Error: No layer priority 3 exists$/)

      layers.rmLayer('layer0')

      assert.strictEqual(layers.length, 2)
      assert.strictEqual(layers.getLayers()['layer0'], undefined)
      const afterRmLayer1 = layers.getLayers()['layer1']
      assert.notStrictEqual(afterRmLayer1, undefined)
      assert.strictEqual(afterRmLayer1.typeSpanData, TypeSpanData.Selection)
      assert.deepEqual(afterRmLayer1.marks, layer1Marks)
      const afterRmLayerText = layers.getLayers()['layerText']
      assert.notStrictEqual(afterRmLayerText, undefined)
      assert.strictEqual(afterRmLayerText.typeSpanData, TypeSpanData.Text)
      assert.strictEqual(layers.getLayers()['layer2'], undefined)

      assert.strictEqual(layers.layerByPriority(0), layer1)
      assert.strictEqual(layers.layerByPriority(1), layerText)
      assert.throws(() => layers.layerByPriority(2), /^Error: No layer priority 2 exists$/)
      assert.throws(() => layers.layerByPriority(3), /^Error: No layer priority 3 exists$/)

      const layer2Marks = [
        numToMark('four five six seven', []),
        numToMark('eight nine', [])
      ]
      layers.addLayer('layer2', TypeSpanData.Text, ...layer2Marks)

      assert.strictEqual(layers.length, 3)
      assert.strictEqual(layers.getLayers()['layer0'], undefined)
      const afterAddLayer1 = layers.getLayers()['layer1']
      assert.notStrictEqual(afterAddLayer1, undefined)
      assert.strictEqual(afterAddLayer1.typeSpanData, TypeSpanData.Selection)
      assert.deepEqual(afterAddLayer1.marks, layer1Marks)
      const afterAddLayerText = layers.getLayers()['layerText']
      assert.notStrictEqual(afterAddLayerText, undefined)
      assert.strictEqual(afterAddLayerText.typeSpanData, TypeSpanData.Text)
      const layer2 = layers.getLayers()['layer2']
      assert.notStrictEqual(layer2, undefined)
      assert.strictEqual(layer2.typeSpanData, TypeSpanData.Text)
      assert.deepEqual(layer2.marks, layer2Marks)

      assert.strictEqual(layers.layerByPriority(0), layer1)
      assert.strictEqual(layers.layerByPriority(1), layerText)
      assert.strictEqual(layers.layerByPriority(2), layer2)
      assert.throws(() => layers.layerByPriority(3), /^Error: No layer priority 3 exists$/)

    })

    it('can modify the layers', function() {

      const layers = new Layers(layerInit)

      const addMark = numToMark('nine', ['sub5', 'sub6'])
      layers.addToLayer('layer0', addMark)
      assert.deepEqual(layers.layerByName('layer0')?.marks, [...layer0Marks, addMark])

      const addPatch = layers.patchByName('layer0')
      assert(!addPatch.empty)
      assert.deepEqual(addPatch.added, [[5, addMark]])
      assert.deepEqual(addPatch.rmed, [])

      const rmMark = numToMark('two', ['sub1'])
      layers.rmFromLayer('layer0', rmMark)
      const expectedMarks = [
        ...layer0Marks.filter(mark => compareMarks(mark, rmMark) !== 0), addMark
      ].sort(compareMarks)
      assert.deepEqual(layers.layerByName('layer0')?.marks, expectedMarks)

      const addAndRmPatch = layers.patchByName('layer0')
      assert(!addAndRmPatch.empty)
      assert.deepEqual(addAndRmPatch.added, [[5, addMark]])
      assert.deepEqual(addAndRmPatch.rmed, [[1, rmMark]])

      layers.resetPatches()
      assert.deepEqual(layers.layerByName('layer0')?.marks, expectedMarks)

      const resetPatch = layers.patchByName('layer0')
      assert(resetPatch.empty)
      assert.deepEqual(resetPatch.added, [])
      assert.deepEqual(resetPatch.rmed, [])

    })

  })

  describe('priority', function() {

    const layerInit: [string, TypeSpanData, IMark[]][] = [
      ['layer0', TypeSpanData.Mark, []],
      ['layer1', TypeSpanData.Mark, []],
      ['layer2', TypeSpanData.Mark, []],
    ]

    it('can permute priority', function() {

      const layers = new Layers(layerInit)
      assert.deepEqual(layers.priorities, ['layer0', 'layer1', 'layer2'])
      layers.permutePriority([1, 0, 2])
      assert.deepEqual(layers.priorities, ['layer1', 'layer0', 'layer2'])
      layers.permutePriority([2, 1, 0])
      assert.deepEqual(layers.priorities, ['layer2', 'layer0', 'layer1'])

    })

    it('can raise layer to front', function() {

      const layers = new Layers(layerInit)
      assert.deepEqual(layers.priorities, ['layer0', 'layer1', 'layer2'])
      layers.layerToFront('layer1')
      assert.deepEqual(layers.priorities, ['layer1', 'layer0', 'layer2'])
      layers.layerToFront('layer2')
      assert.deepEqual(layers.priorities, ['layer2', 'layer1', 'layer0'])

    })

    it('can push layer to back', function() {

      const layers = new Layers(layerInit)
      assert.deepEqual(layers.priorities, ['layer0', 'layer1', 'layer2'])
      layers.layerToBack('layer0')
      assert.deepEqual(layers.priorities, ['layer1', 'layer2', 'layer0'])
      layers.layerToBack('layer2')
      assert.deepEqual(layers.priorities, ['layer1', 'layer0', 'layer2'])

    })

  })

})
