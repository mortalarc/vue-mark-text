import { createApp } from 'vue'

import MarkedTextTest from './MarkedTextTest.vue'
import SubjectTest from './SubjectTest.vue'

const env = (import.meta as any).env
const component = env.VITE_component

if (component === 'subject')
  createApp(SubjectTest, {
    source: env.VITE_source
  }).mount('#app')
else
  createApp(MarkedTextTest, {
    source: env.VITE_source
  }).mount('#app')
