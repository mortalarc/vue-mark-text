---
layout: layouts/page.njk
order: 5
tags: page
title: Misc
---

# Misc

`ts-loader` has been locked to `7.0.4` because of some unholy combination
 of `ts-loader`, `webpack`, `vue-loader`, and `vuepress`.
