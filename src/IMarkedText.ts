import { IManuscript } from './manuscript'
import { IMark } from './IMark'
import { ISubjectEngine, SubjectStyleType } from './subject'

export interface ISubjectComposable {

  subjectEngine: ISubjectEngine
  marks: IMark[]
  subjectStyleType: SubjectStyleType

  updateMarks(): void
  resetColors(): void

  subjectStyle(name: string): any

  // From vue
  $forceUpdate(): void
  get $el(): HTMLElement

}


export interface IManuscriptComposable {

  sourceText: string
  markedTextP: Element
  resetSpans(): void

  manuscript: IManuscript
  refreshManuscript(): void
  setSelection(charRange: [number, number] | null, immediate?: boolean): void
  getSelection(): [number, number] | null

}

export interface IMetaStringComposable {

  manuscript: IManuscript

  metaStringEncloseSections(begSections: number[], endSections: number[]): void
  metaStringDivideBatches(): void
  removeMetaStrings(metaStringType?: string): void

}

export interface IMarkedText extends ISubjectComposable, IManuscriptComposable, IMetaStringComposable { }
