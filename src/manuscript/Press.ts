import arraySort from 'array-sort'
import { BubbleBatchArray, IBatchArray } from 'etali'
import { IMark } from '@/IMark'
import { SpanData, createSpanData, TypeSpanData } from '@/SpanData'
import { Layers } from './Layer'
import { Patch } from './Patch'


export class Press {

  // sourceText is source of truth, assumed to not change
  private sourceText: string

  private syncedSpanBatches: SpanData[][]
  private taskQueue: (() => Promise<void>)[]
  private isQueueRunning: boolean
  private shouldTaskCancel: number

  private layers: Layers

  private heartbeatTime: number
  private batchSyncTimeout: number | null
  private batchSize: number
  private spanDataBatchArray: IBatchArray<SpanData>
  private unpressedPatches: Patch<SpanData>[]


  public constructor(
    sourceText: string, syncedSpanBatches: SpanData[][],
    layers: Layers,
    batchSize: number, heartbeatTime: number = 100
  ) {
    this.sourceText = sourceText
    this.syncedSpanBatches = syncedSpanBatches
    this.taskQueue = []
    this.isQueueRunning = false
    this.shouldTaskCancel = 0
    this.layers = layers
    this.heartbeatTime = heartbeatTime
    this.batchSyncTimeout = null
    this.batchSize = batchSize
    this.spanDataBatchArray = BubbleBatchArray([], this.batchSize)
    this.unpressedPatches = []
    this.produceSpanData()
  }


  public async waitForReady() {
    while (this.taskQueue.length > 0 || this.isQueueRunning) {
      await new Promise(resolve => window.setTimeout(resolve, this.heartbeatTime))
    }
  }


  public async reset() {

    // Cancel current queue
    this.taskQueue = []
    this.shouldTaskCancel += 1
    while (this.isQueueRunning) {
      await new Promise(resolve => window.setTimeout(resolve, this.heartbeatTime))
    }
    this.shouldTaskCancel -= 1

    // Start queue over
    this.produceSpanData()

  }


  private syncSpanBatches() {
    this.batchSyncTimeout = null
    if (this.isQueueRunning && this.batchSyncTimeout == null) {
      this.batchSyncTimeout = window.setTimeout(() => { this.syncSpanBatches() }, this.heartbeatTime)
    } else if (this.syncedSpanBatches !== this.spanDataBatchArray.rawBatches) {

      // Must do a full clone of the spanData so that updates to the object do not trigger a vue update
      const syncBatches = this.syncedSpanBatches
      const rawBatches = this.spanDataBatchArray.rawBatches
      let syncBatchIdx = syncBatches.length - 1
      let rawBatchIdx = rawBatches.length - 1
      let spliceLength = 0
      let spliceItems = [] as SpanData[][]

      while (syncBatchIdx >= 0 && rawBatchIdx >= 0) {
        // This check does not work because we're doing a full clone
        if (
          syncBatches[syncBatchIdx].length === rawBatches[rawBatchIdx].length &&
          syncBatches[syncBatchIdx].every((sb, idx) => sb.equal(rawBatches[rawBatchIdx][idx]))
        ) {
          if (spliceLength > 0 || spliceItems.length > 0) {
            syncBatches.splice(
              syncBatchIdx + 1, spliceLength,
              ...spliceItems.reverse().map(batch => [...batch.map(spanData => spanData.clone())])
            )
            spliceLength = 0
            spliceItems = []
          }
          --syncBatchIdx
          --rawBatchIdx
        } else {
          if (syncBatches[syncBatchIdx].length === 0) {
            ++spliceLength
            --syncBatchIdx
          } else if (rawBatches[rawBatchIdx].length === 0) {
            spliceItems.push([])
            --rawBatchIdx
          } else if (syncBatches[syncBatchIdx][0].charRange[0] < rawBatches[rawBatchIdx][0].charRange[0]) {
            spliceItems.push(rawBatches[rawBatchIdx])
            --rawBatchIdx
          } else if (syncBatches[syncBatchIdx][0].charRange[0] > rawBatches[rawBatchIdx][0].charRange[0]) {
            ++spliceLength
            --syncBatchIdx
          } else {
            ++spliceLength
            spliceItems.push(rawBatches[rawBatchIdx])
            --syncBatchIdx
            --rawBatchIdx
          }
        }
      }
      if (syncBatchIdx >= 0) {
        spliceLength += syncBatchIdx
        syncBatchIdx = 0
      }
      while (rawBatchIdx >= 0) {
        spliceItems.push(rawBatches[rawBatchIdx])
        --rawBatchIdx
      }
      if (spliceLength > 0 || spliceItems.length > 0) {
        syncBatches.splice(
          syncBatchIdx + 1, spliceLength,
          ...spliceItems.reverse().map(batch => [...batch.map(spanData => spanData.clone())])
        )
        spliceLength = 0
        spliceItems = []
      }

      this.unpressedPatches = []

    }
  }

  private runQueue() {
    if (!this.isQueueRunning) {
      this.isQueueRunning = true
      this.asyncRunQueue()
    }
  }

  private async asyncRunQueue() {
    for (let task = this.taskQueue.shift(); task != null; task = this.taskQueue.shift()) {
      await task()
    }
    this.isQueueRunning = false
  }


  public implementPatch() {
    this.patchSpanData()
  }


  // Performance-heavy; use patches after initialization if possible
  private produceSpanData() {

    this.taskQueue.push(async () => {
      this.spanDataBatchArray.rawBatches = []
      this.layers.resetPatches()
      await this.asyncFillSpanBatches()
    })
    this.runQueue()

    if (this.batchSyncTimeout == null) {
      this.batchSyncTimeout = window.setTimeout(() => { this.syncSpanBatches() }, this.heartbeatTime)
    }

  }

  // Use to patch from one SpanData[] to another; resets patch after call
  private patchSpanData() {

    this.taskQueue.push(async () => { await this.asyncPatchSpanData() })
    this.runQueue()

    if (this.batchSyncTimeout == null) {
      this.batchSyncTimeout = window.setTimeout(() => { this.syncSpanBatches() }, this.heartbeatTime)
    }

  }

  private async asyncFillSpanBatches() {

    for await (const spans of this.batchPressLayersBtwn(0, this.sourceText.length)) {

      if (this.shouldTaskCancel) return

      this.spanDataBatchArray.push(...spans)

    }

  }

  // Use to patch from one SpanData[] to another; resets patch after call
  private async asyncPatchSpanData() {

    // Figure out the ranges affected by the patch
    // Figure out the spans overlapping these ranges
    // Recreate these spans from layers
    // Repad the new spans
    // Create patch removing the original spans and adding the new ones

    // Get char ranges that are affected
    const affectedRanges = calcAffectedRanges(this.layers.allPatches())

    // Get spans that overlap with these ranges and any close neighbors as well
    const spanIter = this.spanDataBatchArray.contentIterator()
    const mergedSpanRanges = calcOverlappingSpanRanges(affectedRanges, spanIter)

    // Recreate spanData in mergedSpanRanges from layer
    const spanPatch = new Patch<SpanData>()
    const contentIter = this.spanDataBatchArray.contentIterator()
    let curContent = contentIter.next()
    for (const spanRange of mergedSpanRanges) {

      // Calculate spans from current layers
      const addedSpans = []
      for await (const spans of this.batchPressLayersBtwn(spanRange[0], spanRange[1])) {
        if (this.shouldTaskCancel) return
        addedSpans.push(...spans)
      }

      // Iterate through spanDataArray until we get to the spanRange
      while (!curContent.done && curContent.value[1].charRange[0] < spanRange[0]) {
        curContent = contentIter.next()
      }
      if (curContent.done) break
      // Insert addedSpans at the idx of the first span in spanRange
      const idxChange = curContent.value[0]
      spanPatch.added.push(...addedSpans.map(val => [idxChange, val] as [number, SpanData]))
      // Remove all spans until they are out of spanRange
      while (!curContent.done && (
        curContent.value[1].charRange[1] < spanRange[1] ||
        (
          curContent.value[1].charRange[1] === spanRange[1] &&
          curContent.value[1].charRange[0] < spanRange[1]
        )
      )) {
        spanPatch.rmed.push(curContent.value)
        curContent = contentIter.next()
      }

    }

    this.layers.resetPatches()

    // Transform spanDataBatchArray, but first apply patch index mapping transformation
    //  so that this patch uses coordinates that make sense to the already-transformed array.
    if (!spanPatch.empty) {
      let mappedPatch = spanPatch
      for (const patch of this.unpressedPatches) {
        mappedPatch = mappedPatch.mapIndicesFromBasePatch(patch)
      }
      spanPatch.transform(this.spanDataBatchArray)
      this.unpressedPatches.push(spanPatch)
    }

  }


  private *batchPressLayersBtwn(begChar: number, endChar: number, batchSize: number = 30) {

    const gen = this.pressLayersBtwn(begChar, endChar)
    let iter = gen.next()
    while (!iter.done) {
      const output = []
      for (let i = 0; i < batchSize; ++i) {
        if (iter.value != null) {
          output.push(iter.value)
          iter = gen.next()
        }
      }
      yield output
    }

  }

  private *pressLayersBtwn(begChar: number, endChar: number) {

    let layerInfo = this.layers.layersByPriority().map(
      layer => {
        const idx = layer.marks.findIndex(
          mark => mark.endChar > begChar || (mark.begChar === mark.endChar && mark.begChar === begChar)
        )
        const endIdx = layer.marks.findIndex(mark => mark.begChar >= endChar)
        const mark = idx > -1 ? layer.marks[idx] : null
        const currentMarks: IMark[] = []
        return {
          idx,
          endIdx: endIdx > -1 ? endIdx : layer.marks.length,
          mark,
          currentMarks,
          layer
        }
      }
    )
    layerInfo = layerInfo.filter(info => info.mark != null)

    // Set up initial layerInfo indices
    // loop:
    //  simultaneous:
    //   Find marks ahead involved in constructing the span
    //   Add them to currentMarks
    //   Remove passed marks from currentMarks
    //   Iterate the layerInfo idxs
    //   Find upcomingChar from (sortof)
    //    min(
    //      ...currentMarks.map(mark => mark.endChar),
    //      ...lookaheadMarks.map(mark => mark.begChar)
    //    )
    //  Create SpanData between spanChar and upcomingChar from currentMarks
    //  yield spanData
    //  Move spanChar
    let spanChar = begChar
    while (spanChar < endChar) {

      let isUpcomingSet = false
      let upcomingChar = endChar
      let topLayerMarks: IMark[] = []
      let topLayerType: TypeSpanData = TypeSpanData.Text
      for (const info of layerInfo) {

        // Add marks beg-overlapping [spanChar, _] to currentMarks
        while (info.mark != null && info.mark.begChar <= spanChar) {
          info.currentMarks.push(info.mark)
          info.idx += 1
          if (info.idx === info.endIdx) info.mark = null
          else info.mark = info.layer.marks[info.idx]
        }

        // Remove marks not end-overlapping [skipChar, _]
        const rmIdxs: number[] = []
        // If we set upcomingChar in a higher priority layer, skip to that char
        const skipChar = isUpcomingSet ? upcomingChar : spanChar
        for (let idx = 0; idx < info.currentMarks.length; ++idx) {
          const mark = info.currentMarks[idx]
          if (mark.endChar <= skipChar) {
            rmIdxs.push(idx)
            if (mark.begChar === skipChar) {
              // Zero-width marks are yielded immediately as spans
              const spanText = mark.text ? mark.text : ''
              yield createSpanData(info.layer.typeSpanData, {
                charRange: [mark.begChar, mark.endChar],
                spanText,
                marks: [mark]
              })
            }
          }
        }
        info.currentMarks = info.currentMarks.filter((_, idx) => !rmIdxs.includes(idx))

        // If upcomingChar has not been set in stone, set it if possible
        if (!isUpcomingSet) {
          // Either set it by the upcoming mark's beginning or...
          if (info.mark != null) {
            upcomingChar = Math.min(upcomingChar, info.mark.begChar)
          }
          // ...set it from the current marks' ending
          if (info.currentMarks.length) {
            isUpcomingSet = true
            topLayerMarks = [...info.currentMarks]
            topLayerType = info.layer.typeSpanData
            upcomingChar = Math.min(
              upcomingChar, ...topLayerMarks.map(mark => mark.endChar)
            )
          }
        }

      }

      if (topLayerMarks.length) {
        let spanText = topLayerMarks.find(mark => !!mark.text)?.text
        if (!spanText) spanText = this.sourceText.substring(spanChar, upcomingChar)
        yield createSpanData(topLayerType, {
          charRange: [spanChar, upcomingChar],
          spanText,
          marks: topLayerMarks
        })
      }

      spanChar = upcomingChar

    }

  }

}


function calcAffectedRanges(patches: Patch<IMark>[]) {

  const affectedRanges: [number, number][] = []
  for (const patch of patches) {

    // Get char ranges that are affected
    for (const patchIter of patch.rmThenAddOrder()) {
      const patchRange = [patchIter[1][1].begChar, patchIter[1][1].endChar]
      const overlappingIdxs: number[] = []
      for (const [idx, affectedRange] of affectedRanges.entries()) {
        if (
          (affectedRange[0] < patchRange[1] && patchRange[0] < affectedRange[1]) ||
          (patchRange[0] === patchRange[1] && affectedRange[0] <= patchRange[1] && patchRange[0] <= affectedRange[1])
        ) {
          overlappingIdxs.push(idx)
        }
      }
      const mergeRanges = [patchRange]
      for (const idx of overlappingIdxs.reverse()) {
        mergeRanges.push(affectedRanges.splice(idx, 1)[0])
      }
      affectedRanges.push([
        Math.min(...mergeRanges.map(range => range[0])),
        Math.max(...mergeRanges.map(range => range[1]))
      ])
    }

  }

  arraySort(
    affectedRanges,
    (range0: [number, number], range1: [number, number]) => range0[0] - range1[0]
  )

  return mergeRanges(affectedRanges)

}


function calcOverlappingSpanRanges(
  affectedRanges: [number, number][],
  spanIter: Generator<[number, SpanData]>
) {

  const affectedSpanRanges: [number, number][] = []
  let curSpan: IteratorResult<[number, SpanData]> = spanIter.next()
  let lastSpanData: SpanData | undefined = undefined
  while (!curSpan.done) {
    const spanData = curSpan.value[1]

    if (affectedRanges.some(
      range => range[0] < spanData.charRange[1] && spanData.charRange[0] < range[1]
    )) {
      // If spans overlap with affected ranges, they are affected
      let [begAffected, endAffected] = spanData.charRange
      // If spanData matches edge of affectedRange, go ahead and add the neighbor spanData
      if (lastSpanData && affectedRanges.some(range => range[0] === spanData.charRange[0])) {
        begAffected = lastSpanData.charRange[0]
      }
      lastSpanData = curSpan.value[1]
      curSpan = spanIter.next()
      if (!curSpan.done && affectedRanges.some(range => range[1] === spanData.charRange[1])) {
        endAffected = curSpan.value[1].charRange[1]
      }
      affectedSpanRanges.push([begAffected, endAffected])
    } else {
      if (affectedRanges.some(
        range => range[0] === spanData.charRange[1] || spanData.charRange[0] === range[1]
      )) {
        affectedSpanRanges.push(spanData.charRange)
      }
      lastSpanData = curSpan.value[1]
      curSpan = spanIter.next()
    }
  }

  // Merge span ranges that overlap or neighbor
  return mergeRanges(affectedSpanRanges)

}

function mergeRanges(sortedSpanRanges: [number, number][]) {
  const mergedSpanRanges: [number, number][] = []
  for (const range of sortedSpanRanges) {
    const lastRange = mergedSpanRanges[mergedSpanRanges.length-1]
    if (lastRange && lastRange[1] >= range[0])
      mergedSpanRanges[mergedSpanRanges.length-1][1] = range[1]
    else
      mergedSpanRanges.push([range[0], range[1]])
  }
  return mergedSpanRanges
}
