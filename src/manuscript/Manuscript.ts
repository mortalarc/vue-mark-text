import { reactive } from 'vue'
import { IMark } from '@/IMark'
import { SpanData, TypeSpanData } from '@/SpanData'
import { Layers } from './Layer'
import { type MetaStringType } from './MetaString'
import { Press } from './Press'


export interface IManuscript {

  readonly baseText: string
  selection: [number, number] | null
  batchSize: number

  setSelection(newSelection: [number, number] | null, immediate?: boolean): void
  readonly spanBatches: SpanData[][]

  refresh(): void
  // Try to avoid using, for performance
  resetSpans(): void

  changeMarks(newMarks: IMark[], immediate?: boolean): void

  nodeOffsetToChar(anchorNode: Text, anchorOffset: number): number
  charToSpanIdxOffset(charNum: number): [[number, number], number]
  charToSpanDataOffset(charNum: number): [SpanData, number]
  charToElementOffset(charNum: number): [Element, number]

  metaStringEncloseSections(begSections: number[], endSections: number[]): void
  metaStringDivideBatches(): void
  metaStringClickChar(clickChar: number): void
  removeMetaStrings(metaStringType?: MetaStringType): void

}


/*
  This class manages multiple layers of text.
  It uses a SpanDepot to translate layer activity & priority into batches of spans.
  It keeps as one layer the selection.
*/
export class Manuscript implements IManuscript {

  private text: string
  private layers: Layers
  private markedTextPGetter: () => Element
  private depotBatchSize: number
  private selectionCharRange: [number, number] | null

  private syncedSpanBatches: SpanData[][]
  private press: Press
  private metaStringLookup: Map<MetaStringType, [string, number][]>


  public constructor(
    text: string,
    marks: IMark[],
    markedTextPGetter: () => Element,
    batchSize: number = 20,
    textSpanMaxLength: number = 100000
  ) {

    this.text = text

    const textMarks = []
    let lastSpanEnd = 0
    for (const spanEnd of splitIntegerEvenly(text.length, textSpanMaxLength)) {
      textMarks.push(
        { begChar: lastSpanEnd, endChar: spanEnd, subjects: [] }
      )
      lastSpanEnd = spanEnd
    }

    this.layers = new Layers([
      ['selection', TypeSpanData.Selection, []],
      ['meta', TypeSpanData.Meta, []],
      ['mark', TypeSpanData.Mark, marks],
      ['base', TypeSpanData.Text, textMarks]
    ])
    this.markedTextPGetter = markedTextPGetter
    this.depotBatchSize = batchSize
    this.selectionCharRange = null

    this.syncedSpanBatches = reactive([])
    this.press = new Press(
      this.text, this.syncedSpanBatches,
      this.layers, this.depotBatchSize
    )

    this.metaStringLookup = new Map<MetaStringType, [string, number][]>()

  }

  public async waitForReady() {
    await this.press.waitForReady()
  }


  public get baseText() {
    return this.text
  }

  public get batchSize() {
    return this.depotBatchSize
  }
  public set batchSize(val) {
    this.depotBatchSize = val
  }


  public get selection() {
    return this.selectionCharRange
  }
  public set selection(newSelection: [number, number] | null) {
    this.setSelection(newSelection)
  }
  public setSelection(newSelection: [number, number] | null) {
    this.selectionCharRange = newSelection
    const newMarks = newSelection == null ? [] : [{
      begChar: newSelection[0],
      endChar: newSelection[1],
      subjects: []
    }]
    this.layers.changeLayer('selection', newMarks)
    const sel = window.getSelection()
    if (sel) sel.removeAllRanges()
  }

  public get spanBatches() {
    return this.syncedSpanBatches
  }


  public refresh() {
    // Try to call this only from Controller so batches are synced ONCE
    this.press.implementPatch()
  }


  // Try to avoid using, for performance
  public resetSpans() {
    this.press.reset()
  }


  public changeMarks(newMarks: IMark[]) {
    this.layers.changeLayer('mark', newMarks)
  }


  public nodeOffsetToChar(anchorNode: Text, anchorOffset: number) {

    const spanNode = anchorNode.parentElement
    if (spanNode == null) {
      throw Error('Anchor not the child of a span')
    }

    let batchIdx = 0
    let sibling = spanNode.previousElementSibling
    while (sibling != null) {
      sibling = sibling.previousElementSibling
      ++batchIdx
    }
    let depotIdx = 0
    sibling = spanNode.parentElement?.previousElementSibling || null
    while (sibling != null) {
      sibling = sibling.previousElementSibling
      ++depotIdx
    }

    const spanData = this.spanBatches.at(depotIdx)?.at(batchIdx)
    if (!spanData) {
      throw Error(`No matching spanData found for D${depotIdx}:B${batchIdx}`)
    }

    return spanData.charRange[0] + anchorOffset

  }

  public charToSpanIdxOffset(charNum: number) {

    if (this.spanBatches.length === 0) {
      throw Error(`No spanBatches to find char ${charNum}`)
    }

    let depotIdx = 0
    while (
      depotIdx < this.spanBatches.length - 1 &&
      this.spanBatches[depotIdx+1].length &&
      this.spanBatches[depotIdx+1][0].charRange[0] <= charNum
    ) {
      ++depotIdx
    }
    const batch = this.spanBatches[depotIdx]

    if (batch.length === 0) {
      throw Error(`spanBatch to find char ${charNum} empty`)
    }

    let batchIdx = 0
    while (
      batchIdx < batch.length - 1 &&
      batch[batchIdx+1].charRange[0] <= charNum
    ) {
      ++batchIdx
    }
    const spanData = batch[batchIdx]

    if (spanData.charRange[1] < charNum) {
      throw Error(`charNum ${charNum} did not match spanData charRange ${spanData.charRange}`)
    }

    return [[depotIdx, batchIdx], charNum - spanData.charRange[0]] as [[number, number], number]

  }

  public charToSpanDataOffset(charNum: number) {
    const [[depotIdx, batchIdx], offset] = this.charToSpanIdxOffset(charNum)
    return [this.spanBatches[depotIdx][batchIdx], offset] as [SpanData, number]
  }

  public charToElementOffset(charNum: number) {
    const [[depotIdx, batchIdx], offset] = this.charToSpanIdxOffset(charNum)
    const htmlP = this.markedTextPGetter()
    const htmlSpan = htmlP.children.item(depotIdx)?.children.item(batchIdx)
    if (!htmlSpan) {
      throw Error(`No matching htmlSpan found for D${depotIdx}:B${batchIdx}`)
    }
    return [htmlSpan, offset] as [Element, number]
  }


  public metaStringEncloseSections(begSections: number[], endSections: number[]) {

    const metaChars = [
      ...begSections.map(idx => ['[', idx] as [string, number]),
      ...endSections.map(idx => [']', idx] as [string, number]),
    ]
    if (!this.metaStringLookup.has('enclose-sections'))
      this.metaStringLookup.set('enclose-sections', [])
    this.metaStringLookup.get('enclose-sections')?.push(...metaChars)

    this.refreshMetaStrings()

  }

  public metaStringDivideBatches() {

    const metaChars = Array(...this.spanBatches.filter(
      batch => batch.length > 0
    ).map(
      batch => ['|', batch[0].charRange[0]] as [string, number]
    ))
    if (!this.metaStringLookup.has('divide-batches'))
      this.metaStringLookup.set('divide-batches', [])
    this.metaStringLookup.get('divide-batches')?.push(...metaChars)

    this.refreshMetaStrings()

  }

  public metaStringClickChar(clickChar: number) {
    if (!this.metaStringLookup.has('click-char'))
      this.metaStringLookup.set('click-char', [])
    this.metaStringLookup.get('click-char')?.push(['|', clickChar])
    this.refreshMetaStrings()
  }

  public removeMetaStrings(metaStringType?: MetaStringType) {
    if (metaStringType == null) {
      // If metaStringType is undefined, clear all metaStrings
      this.metaStringLookup.clear()
    } else {
      // Otherwise, only clear metaStrings of that type
      this.metaStringLookup.delete(metaStringType)
    }
    this.refreshMetaStrings()
  }

  public setMetaStrings(metaStrings: [string, number][]) {
    const newMarks = metaStrings.map(metaString => ({
      begChar: metaString[1],
      endChar: metaString[1],
      text: metaString[0],
      subjects: []
    }))
    this.layers.changeLayer('meta', newMarks)
  }

  private refreshMetaStrings() {
    const metaChars = ([] as [string, number][]).concat(...this.metaStringLookup.values())
    this.setMetaStrings(metaChars)
  }

}


function* splitIntegerEvenly(big: number, maxChunk: number) {
  const numTextMarks = Math.ceil(big / maxChunk)
  const actualSpanSize = big / numTextMarks
  const spanBiggerNum = big % numTextMarks
  let spanChar = 0
  for (let i = 0; i < spanBiggerNum; ++i) {
    spanChar = spanChar + actualSpanSize + 1
    yield spanChar
  }
  for (let i = spanBiggerNum; i < numTextMarks; ++i) {
    spanChar = spanChar + actualSpanSize
    yield spanChar
  }
}