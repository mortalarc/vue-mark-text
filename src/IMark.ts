/*
IMark interface:
  begChar - beginning char index in the source text
  endChar - end char index in the source text
  text - text that will be displayed (probably should but doesn't have to be sourceText.slice(begChar, endChar))
  subjects - strings used for color, leader lines, filtering, etc.
*/
export interface IMark {
  begChar: number
  endChar: number
  text?: string
  subjects: string[]
}

// Note: does not compare subjects!
export function compareMarks(mark1: IMark, mark2: IMark) {
  return (mark1.begChar - mark2.begChar) ||
    (mark1.endChar - mark2.endChar) ||
    ((mark1.text ? 1: 0) - (mark2.text ? 1 : 0)) ||
    (mark1.text && mark2.text && mark1.text.localeCompare(mark2.text)) ||
    0
}
