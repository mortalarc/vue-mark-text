---
layout: layouts/page.njk
order: 1
tags: page
title: Example
---

# Example

The following example is exactly the Vue component found in
 `test/MarkedTextTest.vue`.

<div id="app"/>
<script src="/vue-mark-text/js/main.js"></script>
