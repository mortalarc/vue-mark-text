type MetaStringType = 'divide-batches' | 'enclose-sections' | 'click-char'

export {
  MetaStringType
}
