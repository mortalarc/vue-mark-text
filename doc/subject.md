---
layout: layouts/page.njk
order: 3
tags: page
title: Subject
---

# Subject

The subjects can be controlled from access to the `MarkedText`,
 using methods such as `toggleSubject` or `lineConnectSubject`.
Below shows some access to these methods, mapped to buttons.

`colorOptions` and `subjectStyleType` control the look of subjects.
We use the [`color-scheme`](https://www.npmjs.com/package/color-scheme) package
 to manage color options (this may change in the near future).
`colorOptions` derives its parameters from this usage.
`subjectStyleType` controls how these colors are derived and their relationship;
 the default gives 16 relatively distinct colors.

We also implement connecting lines between marks that share the same subject.
This feature is fairly prototypical, with little option for customization.
Suggestions for improvement and use-cases are encouraged.

The following example is the Vue component found in
 `test/SubjectTest.vue`.

<div id="app"/>
<script src="/vue-mark-text/js/subject.js"></script>
