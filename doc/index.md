---
layout: layouts/page.njk
order: 0
tags: page
title: vue-mark-text
---

# vue-mark-text

Highlight/mark text based on character indices.

Supply text along with `marks`,
 an iterable of objects with `begChar`, `endChar`, `text`, and (optional) `subject`,
 where `begChar`/`endChar` are character indices in the text.
The component will split the text into batches of spans.
The `subject` string along with customizable `colorOptions` and `subjectStyleType`
 parameters is used to control the style of the text.
This automatically handles overlap by
 merging classes (see subpackage `etali` for batch and merging
 implementations).

A `controller` may also be supplied to catch the events from
 clicking on the text spans and trigger callback.
Three controllers are provided in the library.
By default, `NullController` has no callback.
`TextSelectionController` consumes clicks and drags, annotating
 by dragging over text as one would expect and also automatically
 expanding to an entire mark if it is clicked on without dragging.
`WordSelectionController` behaves similarly, with the added functionality
 that it expands selection to the nearest word boundaries on raw text
 clicks and drags.

`vue-mark-text` natively handles selection using spans to interfere
 as little as possible with the rest of the application.
The selection may be controlled directly in MarkedText using the methods
 `get`/`set`/`reset``Selection`, or through a controller like WordSelectionController.
The native selection specifically deals with text char indices and not the DOM.

MarkedText is used like:

``` html
<MarkedText id="mt" :sourceText="sourceText" :marks="marks" :controller="controller"/>
```
