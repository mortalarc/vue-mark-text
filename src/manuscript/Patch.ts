import { IBatchArray } from 'etali'

export enum PatchType {
  add, rm
}

export class Patch<T> {

  // All indices refer to the previous sortedMarks
  // These MUST be sorted by first element (and in desired addition order)
  public added: [number, T][]
  public rmed: [number, T][]


  public constructor(added: [number, T][] = [], rmed: [number, T][] = []) {
    this.added = added
    this.rmed = rmed
  }


  public get empty() {
    return this.added.length === 0 && this.rmed.length === 0
  }
  public reset() {
    this.added = []
    this.rmed = []
  }


  public transform(array: T[] | IBatchArray<T>): T[] | IBatchArray<T> {

    const splices: [number, number, ...T[]][] = []

    const iter = this.rmThenAddOrder()
    let elem = iter.next()
    while (!elem.done) {
      const idx = elem.value[1][0]

      if (elem.value[0] === PatchType.add) {

        const additions = []
        while (!elem.done && elem.value[1][0] === idx) {
          additions.push(elem.value[1][1])
          elem = iter.next()
        }
        splices.push([idx, 0, ...additions])

      } else {

        let curIdx = idx
        const additions = []
        while (!elem.done && elem.value[1][0] <= curIdx) {
          switch(elem.value[0]) {
          case PatchType.add:
            additions.push(elem.value[1][1])
            break
          case PatchType.rm:
            curIdx += 1
            break
          }
          elem = iter.next()
        }
        splices.push([idx, curIdx - idx, ...additions])

      }

    }

    // Apply splices in reverse order
    for (const splice of splices.reverse()) {
      array.splice(...splice)
    }

    return array

  }

  public *rmThenAddOrder() {
    const addedIter = this.added[Symbol.iterator]()
    const rmedIter = this.rmed[Symbol.iterator]()
    let added = addedIter.next(), rmed = rmedIter.next()
    while (!added.done || !rmed.done) {
      if (
        !rmed.done && (added.done || rmed.value[0] <= added.value[0])
      ) {
        yield [PatchType.rm, rmed.value] as [PatchType, [number, T]]
        rmed = rmedIter.next()
      } else {
        yield [PatchType.add, added.value] as [PatchType, [number, T]]
        added = addedIter.next()
      }
    }
  }

  public *addThenRmOrder() {
    const addedIter = this.added[Symbol.iterator]()
    const rmedIter = this.rmed[Symbol.iterator]()
    let added = addedIter.next(), rmed = rmedIter.next()
    while (!added.done || !rmed.done) {
      if (
        !rmed.done && (added.done || rmed.value[0] < added.value[0])
      ) {
        yield [PatchType.rm, rmed.value] as [PatchType, [number, T]]
        rmed = rmedIter.next()
      } else {
        yield [PatchType.add, added.value] as [PatchType, [number, T]]
        added = addedIter.next()
      }
    }
  }


  // Same as static function
  public merge(other: Patch<T>): Patch<T> {
    return mergePatches(this, other)
  }

  // Same as static function
  public mapIndices(mapper: PatchIndexMapper) {
    return mapPatchIndices(this, mapper)
  }

  // Same as static functions together
  public mapIndicesFromBasePatch<U>(basePatch: Patch<U>): Patch<T> {
    return this.mapIndices(patchIndexMapper(basePatch))
  }

}


export function mergePatches<T>(patch0: Patch<T>, patch1: Patch<T>) {

  const mergedPatch = new Patch<T>()

  const iter0 = patch0.rmThenAddOrder()
  const iter1 = patch1.rmThenAddOrder()
  let elem0 = iter0.next(), elem1 = iter1.next(), idxDiffTot = 0
  while (!elem0.done || !elem1.done) {
    const elem0Idx = elem0.done ? 0 : elem0.value[1][0]
    const elem1Idx = elem1.done ? 0 : elem1.value[1][0] + idxDiffTot

    if (!elem0.done && (elem1.done || elem0Idx < elem1Idx)) {

      switch(elem0.value[0]) {
      case PatchType.add:
        idxDiffTot -= 1
        mergedPatch.added.push([elem0Idx, elem0.value[1][1]])
        break
      case PatchType.rm:
        idxDiffTot += 1
        mergedPatch.rmed.push([elem0Idx, elem0.value[1][1]])
        break
      }
      elem0 = iter0.next()

    } else if (!elem1.done && (elem0.done || elem1Idx < elem0Idx)) {

      switch(elem1.value[0]) {
      case PatchType.add:
        mergedPatch.added.push([elem1Idx, elem1.value[1][1]])
        break
      case PatchType.rm:
        mergedPatch.rmed.push([elem1Idx, elem1.value[1][1]])
        break
      }
      elem1 = iter1.next()

    } else {
      // !elem0.done && !elem1.done && elem0Idx === elem1Idx
      // Merge in the following order:
      // add1 -> add0 -> rm0 -> rm1

      const goodElem0 = elem0.value as [PatchType, [number, T] | [number, T, T]]
      const goodElem1 = elem1.value as [PatchType, [number, T] | [number, T, T]]
      const type0 = goodElem0[0], type1 = goodElem1[0]
      if (type1 === PatchType.add) {
        mergedPatch.added.push([elem1Idx, goodElem1[1][1]])
        elem1 = iter1.next()
      } else if (type0 === PatchType.add) {
        idxDiffTot -= 1
        mergedPatch.added.push([elem0Idx, goodElem0[1][1]])
        elem0 = iter0.next()
      } else if (type0 === PatchType.rm) {
        idxDiffTot += 1
        mergedPatch.added.push([elem0Idx, goodElem0[1][1]])
        elem0 = iter0.next()
      } else if (type1 === PatchType.rm) {
        mergedPatch.added.push([elem1Idx, goodElem1[1][1]])
        elem1 = iter1.next()
      }

    }

  }

  // TODO: Remove matching rm & add

  return mergedPatch

}


export type PatchIndexMapper = (patchType: PatchType, index: number) => number

// Covariant/contravariant index mapping
// Note: does not deal with overlap!
export function patchIndexMapper<T>(base: Patch<T>): PatchIndexMapper {

  const firstElem = base.rmThenAddOrder().next()
  if (firstElem.value == null) return x => x
  let currentChange: [number, number] = [firstElem.value[1][0] - 1, 0]

  const accumulatedChanges: [number, number][] = []
  for (const elem of base.rmThenAddOrder()) {
    if (elem[1][0] !== currentChange[0]) {
      currentChange = [elem[1][0], currentChange[1]]
      accumulatedChanges.push(currentChange)
    }
    currentChange[1] += (elem[0] === PatchType.add) ? 1 : -1
  }

  // Reverse the accumulated changes for easier find (unless binary search)
  accumulatedChanges.reverse()

  return (patchType: PatchType, index: number) => {
    // Could get speedup with binary search if necessary
    let geqIdx = accumulatedChanges.findIndex(val => val[0] <= index)
    if (geqIdx !== -1 && accumulatedChanges[geqIdx][0] === index && patchType === PatchType.add) {
      // If it finds an index that is exactly equal and patch type is rm,
      //  use next (lower index) accumulated change instead
      geqIdx += 1
    }
    if (geqIdx === -1 || geqIdx === accumulatedChanges.length) return index
    else return index + accumulatedChanges[geqIdx][1]
  }

}


export function mapPatchIndices<T>(origPatch: Patch<T>, mapper: PatchIndexMapper): Patch<T> {
  return new Patch(
    origPatch.added.map(val => [mapper(PatchType.add, val[0]), val[1]]),
    origPatch.rmed.map(val => [mapper(PatchType.rm, val[0]), val[1]])
  )
}
