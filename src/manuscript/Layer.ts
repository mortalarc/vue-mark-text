import arraySort from 'array-sort'
import { IMark, compareMarks } from '@/IMark'
import { TypeSpanData } from '@/SpanData'
import { Patch, mergePatches } from './Patch'


interface ILayer {
  typeSpanData: TypeSpanData
  marks: IMark[]
}


// This class should take in marks and output padded spans from merged layers
export class Layers {

  // all names for layers
  private layerNames: string[]
  // layers is a map from layer name to sorted marks
  private layers: Record<string, ILayer>
  // diffs between layers at last refreshComposite and current; sorted by idx
  private layerPatches: Record<string, Patch<IMark>>
  // priorities stores the layer names in priority order
  private layerPriorities: string[]

  public constructor(layers: Iterable<[string, TypeSpanData, IMark[]]>) {
    this.layerNames = []
    this.layers = { }
    this.layerPatches = { }
    this.layerPriorities = []
    for (const layer of layers) {
      const layerName = layer[0]
      if (this.layerNames.includes(layerName)) throw Error('Layers should not share names')
      this.layerNames.push(layerName)
      const sortedMarks = arraySort(layer[2].slice(), ['begChar', 'endChar', 'text', 'subjects'])
      this.layers[layerName] = { typeSpanData: layer[1], marks: sortedMarks }
      this.layerPatches[layerName] = new Patch()
      this.layerPriorities.push(layerName)
    }
  }


  public addLayer(name: string, typeSpanData: TypeSpanData, ...marks: IMark[]) {
    if (!this.layerNames.includes(name)) {
      this.layerNames.push(name)
      this.layers[name] = { typeSpanData, marks: [] }
      this.layerPatches[name] = new Patch()
      this.layerPriorities.push(name)
    }
    this.addToLayer(name, ...marks)
  }
  public rmLayer(name: string) {
    if (this.layerNames.includes(name)) {
      this.layerNames.splice(this.layerNames.indexOf(name), 1)
      delete this.layers[name]
      delete this.layerPatches[name]
      this.layerPriorities.splice(this.layerPriorities.indexOf(name), 1)
    }
  }

  public resetPatches() {
    this.layerNames.forEach(name => {
      this.layerPatches[name] = new Patch()
    })
  }


  public get length() { return this.layerNames.length }

  public layerByName(name: string) {
    return this.layers[name] || undefined
  }
  public getLayers() {
    return this.layers
  }

  public patchByName(name: string) {
    return this.layerPatches[name]
  }
  public getPatches() {
    return this.layerPatches
  }
  public allPatches() {
    return this.layerNames.map(name => this.layerPatches[name])
  }

  public get priorities() { return this.layerPriorities }

  public layerByPriority(priority: number) {
    if (this.layerPriorities.length <= priority) throw Error('No layer priority ' + priority + ' exists')
    return this.layers[this.layerPriorities[priority]]
  }
  public layersByPriority() {
    return this.layerPriorities.map(name => this.layers[name])
  }

  public permutePriority(permutation: number[]) {
    if (
      permutation.length !== this.layerNames.length ||
      !Array.from({ length: permutation.length }, (_, i) => i).every(num => permutation.includes(num))
    ) throw new Error('Supply valid priority permutation')
    this.layerPriorities.splice(
      0, this.layerPriorities.length,
      ...this.layerPriorities.map((_, idx) => this.layerPriorities[permutation[idx]])
    )
  }
  public layerToFront(layerName: string) {
    const priority = this.layerPriorities.indexOf(layerName)
    if (priority === -1) throw new Error('Unknown layer ' + layerName)
    this.layerPriorities.splice(
      0, priority + 1,
      layerName, ...this.layerPriorities.slice(0, priority)
    )
  }
  public layerToBack(layerName: string) {
    const priority = this.layerPriorities.indexOf(layerName)
    if (priority === -1) throw new Error('Unknown layer ' + layerName)
    this.layerPriorities.splice(
      priority, this.layerPriorities.length - priority,
      ...this.layerPriorities.slice(priority + 1, this.layerPriorities.length),
      layerName
    )
  }


  // Change all marks in a layer
  // Note: DOES NOT refresh composite, so it can be batched
  public changeLayer(name: string, newMarks: IMark[]) {

    const newSortedMarks = arraySort(newMarks.slice(), ['begChar', 'endChar', 'text', 'subjects'])
    const newPatch = this.patchFromDiff(this.layerByName(name).marks, newSortedMarks)
    this.layers[name].marks = newSortedMarks

    this.mergePatch(name, newPatch)

  }

  // Add marks to a layer
  // Note: DOES NOT refresh composite, so it can be batched
  public addToLayer(name: string, ...addMarks: IMark[]) {

    const addSortedMarks = arraySort(addMarks.slice(), ['begChar', 'endChar', 'text', 'subjects'])
    const newPatch = this.patchFromAdd(this.layerByName(name).marks, addSortedMarks)
    newPatch.transform(this.layers[name].marks)

    this.mergePatch(name, newPatch)

  }

  // Rm marks from a layer
  // Note: DOES NOT refresh composite, so it can be batched
  public rmFromLayer(name: string, ...rmMarks: IMark[]) {

    const rmSortedMarks = arraySort(rmMarks.slice(), ['begChar', 'endChar', 'text', 'subjects'])
    const newPatch = this.patchFromRm(this.layerByName(name).marks, rmSortedMarks)
    newPatch.transform(this.layers[name].marks)

    this.mergePatch(name, newPatch)

  }


  private mergePatch(layerName: string, newPatch: Patch<IMark>) {
    const mergedPatch = mergePatches(this.layerPatches[layerName], newPatch)
    this.layerPatches[layerName] = mergedPatch
    // TODO: Make sure subjects change correctly
  }

  private patchFromDiff(oldSortedMarks: IMark[], newSortedMarks: IMark[]) {

    const newPatch = new Patch<IMark>()
    let oldIdx = 0, newIdx = 0
    while (oldIdx < oldSortedMarks.length && newIdx < newSortedMarks.length) {
      const oldMark = oldSortedMarks[oldIdx]
      const newMark = newSortedMarks[newIdx]
      const compare = compareMarks(oldMark, newMark)

      if (compare < 0) {
        newPatch.rmed.push([oldIdx, oldMark])
        ++oldIdx
      } else if (compare > 0) {
        newPatch.added.push([oldIdx, newMark])
        ++newIdx
      } else {
        if (
          oldMark.subjects.some(sub => !newMark.subjects.includes(sub)) ||
          newMark.subjects.some(sub => !oldMark.subjects.includes(sub))
        ) {
          newPatch.rmed.push([oldIdx, oldMark])
          newPatch.added.push([oldIdx, newMark])
        }
        ++oldIdx
        ++newIdx
      }

    }

    while (oldIdx < oldSortedMarks.length) {
      newPatch.rmed.push([oldIdx, oldSortedMarks[oldIdx]])
      ++oldIdx
    }
    while (newIdx < newSortedMarks.length) {
      newPatch.added.push([oldIdx, newSortedMarks[newIdx]])
      ++newIdx
    }

    return newPatch

  }

  private patchFromAdd(sortedMarks: IMark[], addedSortedMarks: IMark[]) {

    const newPatch = new Patch<IMark>()
    let oldIdx = 0, newIdx = 0
    while (oldIdx < sortedMarks.length && newIdx < addedSortedMarks.length) {
      const oldMark = sortedMarks[oldIdx]
      const newMark = addedSortedMarks[newIdx]
      const compare = compareMarks(oldMark, newMark)
      if (compare > 0) {
        newPatch.added.push([oldIdx, newMark])
        ++newIdx
      } else {
        ++oldIdx
      }
    }

    while (newIdx < addedSortedMarks.length) {
      newPatch.added.push([oldIdx, addedSortedMarks[newIdx]])
      ++newIdx
    }

    return newPatch

  }

  private patchFromRm(sortedMarks: IMark[], rmedSortedMarks: IMark[]) {

    const newPatch = new Patch<IMark>()
    let oldIdx = 0, newIdx = 0
    while (oldIdx < sortedMarks.length && newIdx < rmedSortedMarks.length) {
      const oldMark = sortedMarks[oldIdx]
      const newMark = rmedSortedMarks[newIdx]
      const compare = compareMarks(oldMark, newMark)

      if (
        compare === 0 &&
        oldMark.subjects.every(sub => newMark.subjects.includes(sub)) &&
        newMark.subjects.every(sub => oldMark.subjects.includes(sub))
      ) {
        newPatch.rmed.push([oldIdx, oldMark])
        ++oldIdx
        ++newIdx
      } else if (compare > 0) {
        console.error('cannot find mark to remove; skipping:')
        console.error(newMark)
        ++newIdx
      } else {
        ++oldIdx
      }

    }

    while (newIdx < rmedSortedMarks.length) {
      console.error('cannot find mark to remove; skipping:')
      console.error(rmedSortedMarks[newIdx])
      ++newIdx
    }

    return newPatch

  }

}
