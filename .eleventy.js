const syntaxHighlight = require('@11ty/eleventy-plugin-syntaxhighlight')

module.exports = function(eleventyConfig) {

  eleventyConfig.addPlugin(syntaxHighlight)

  // Create an array of all tags
  eleventyConfig.addCollection("tagList", function(collection) {
    let tagSet = new Set()
    collection.getAll().forEach(item => {
      (item.data.tags || []).forEach(tag => tagSet.add(tag))
    })

    return [...tagSet]
  })

  // Sort pages by the "order" property
  eleventyConfig.addCollection('sortedPage', function(collectionApi) {
    return collectionApi.getFilteredByTag('page').sort(
      (page0, page1) => page0.data.order - page1.data.order
    )
  })

  // Copy the `css` folder to the output
  eleventyConfig.addPassthroughCopy("_includes/css")

  return {

    // These are all optional (defaults are shown):
    dir: {
      input: "doc",
      includes: "_includes",
      data: "_data",
      output: "doc/dist"
    },

    // Control which files Eleventy will process
    // e.g.: *.md, *.njk, *.html, *.liquid
    templateFormats: [
      "md",
      "njk",
      "html",
      "css"
    ],

    // -----------------------------------------------------------------
    // If your site deploys to a subdirectory, change `pathPrefix`.
    // Don’t worry about leading and trailing slashes, we normalize these.

    // If you don’t have a subdirectory, use "" or "/" (they do the same thing)
    // This is only used for link URLs (it does not affect your file structure)
    // Best paired with the `url` filter: https://www.11ty.dev/docs/filters/url/

    // You can also pass this in on the command line using `--pathprefix`

    // Optional (default is shown)
    pathPrefix: "/vue-mark-text/",
    // -----------------------------------------------------------------

    // Pre-process *.md files with: (default: `liquid`)
    markdownTemplateEngine: "njk",

    // Pre-process *.html files with: (default: `liquid`)
    htmlTemplateEngine: "njk",

    // Opt-out of pre-processing global data JSON files: (default: `liquid`)
    dataTemplateEngine: false

  }
}
