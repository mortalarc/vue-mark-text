import { IMark } from './IMark'
import { IManuscriptComposable } from './IMarkedText'


export interface IController {
  link(composable: IManuscriptComposable): void
  unlink(): void
  deselect(): void
  selectNew(begChar: number, endChar: number): void
  selectMarks(marks: IMark[]): void
  onMouseUpText(e: Event, charRange: [number, number]): void
  onMouseUpMark(e: Event, charRange: [number, number], marks: IMark[]): void
  onMouseUpSelection(e: Event, charRange: [number, number]): void
  onMouseUpMetaString(e: Event, charRange: [number, number], metaStringText: string): void
  onMouseUpOther(e: Event): void
}


export abstract class AController implements IController {
  protected composable: IManuscriptComposable | null = null
  public link(composable: IManuscriptComposable) {
    if (this.composable !== composable) {
      this.deselect()
      this.composable = composable
    }
  }
  public unlink() {
    if (this.composable != null) {
      this.deselect()
      this.composable = null
    }
  }

  public abstract deselect(): void
  public abstract selectNew(begChar: number, endChar: number): void
  public abstract selectMarks(marks: IMark[]): void

  public onMouseUpText(e: Event, charRange: [number, number]): void {
    this.xOnMouseUpText(e, charRange)
    this.composable?.refreshManuscript()
  }
  protected abstract xOnMouseUpText(e: Event, charRange: [number, number]): void
  public onMouseUpMark(e: Event, charRange: [number, number], marks: IMark[]): void {
    this.xOnMouseUpMark(e, charRange, marks)
    this.composable?.refreshManuscript()
  }
  protected abstract xOnMouseUpMark(e: Event, charRange: [number, number], marks: IMark[]): void
  public onMouseUpSelection(e: Event, charRange: [number, number]): void {
    this.xOnMouseUpSelection(e, charRange)
    this.composable?.refreshManuscript()
  }
  protected abstract xOnMouseUpSelection(e: Event, charRange: [number, number]): void
  public onMouseUpMetaString(e: Event, charRange: [number, number], metaStringText: string): void {
    this.xOnMouseUpMetaString(e, charRange, metaStringText)
    this.composable?.refreshManuscript()
  }
  protected abstract xOnMouseUpMetaString(e: Event, charRange: [number, number], metaStringText: string): void
  public onMouseUpOther(e: Event): void {
    this.xOnMouseUpOther(e)
    this.composable?.refreshManuscript()
  }
  protected abstract xOnMouseUpOther(e: Event): void
}


class NullController extends AController {
  public deselect() { return }
  public selectNew() { return }
  public selectMarks() { return }
  protected xOnMouseUpText() { return }
  protected xOnMouseUpMark() { return }
  protected xOnMouseUpSelection() { return }
  protected xOnMouseUpMetaString() { return }
  protected xOnMouseUpOther() { return }
}


class TextSelectionController extends AController {

  private deselectCbk: () => void
  private selectNewCbk: (begChar: number, endChar: number) => void
  private selectMarksCbk: (marks: IMark[]) => void

  public constructor(
    deselectCbk: TextSelectionController['deselectCbk'],
    selectNewCbk: TextSelectionController['selectNewCbk'],
    selectMarksCbk: TextSelectionController['selectMarksCbk']
  ) {
    super()
    this.deselectCbk = deselectCbk
    this.selectNewCbk = selectNewCbk
    this.selectMarksCbk = selectMarksCbk
  }


  public deselect() {
    if (this.composable) this.composable.setSelection(null, false)
    this.deselectCbk()
  }

  public selectNew(begChar: number, endChar: number) {
    if (this.composable) this.composable.setSelection([begChar, endChar], false)
    this.selectNewCbk(begChar, endChar)
  }

  public selectMarks(marks: IMark[]) {
    if (this.composable) {
      if (marks.length === 1)
        this.composable.setSelection([marks[0].begChar, marks[0].endChar], false)
      else
        this.composable.setSelection(null, false)
    }
    this.selectMarksCbk(marks)
  }



  protected xOnMouseUpText(e: Event, charRange: [number, number]) {

    e.stopPropagation()

    // Grab selection and determine beg/end char
    const selection = window.getSelection()
    if (!selection) return

    if (!(selection.anchorNode instanceof Text)
     || !(selection.focusNode instanceof Text)) {
      console.error(
        'Window selection behaving strangely; anchor or focus not a Text.'
      )
      this.deselect()
      return
    }

    let isReversed = false
    const range = document.createRange()
    range.setStart(selection.anchorNode, selection.anchorOffset)
    range.setEnd(selection.focusNode, selection.focusOffset)

    if (range.compareBoundaryPoints(Range.START_TO_END, range) !== 1) {
      // If end < start, reverse them
      isReversed = true
      range.setStart(selection.focusNode, selection.focusOffset)
      range.setEnd(selection.anchorNode, selection.anchorOffset)
    }

    if (range.toString().length === 0) {
      if ((selection.focusNode as Text).length > selection.focusOffset) {
        range.setEnd(range.endContainer, range.endOffset + 1)
      }
    }

    let begChar = range.startOffset
    let endChar = range.endOffset

    // Find chars between doc start and range start; recalibrate beg/endChar
    const textLength = range.toString().length
    if (isReversed) {
      begChar += charRange[0]
      endChar = begChar + textLength
    } else {
      endChar += charRange[0]
      begChar = endChar - textLength
    }

    this.selectNew(begChar, endChar)

  }

  protected xOnMouseUpMark(e: Event, charRange: [number, number], marks: IMark[]) {

    e.stopPropagation()

    const selection = window.getSelection()
    if (!selection) return

    // If non-empty text selection, delegate to onMouseUp
    if (selection.getRangeAt(0).toString().length !== 0) {
      this.onMouseUpText(e, charRange)
      return
    }

    // Calculate ML range from mark
    const range = document.createRange()

    let startNode = e.target
    while (!(startNode instanceof Text)) {
      startNode = (startNode as Node).childNodes[0]
    }
    range.setStart(startNode, 0)

    let endNode = e.target
    while (!(endNode instanceof Text)) {
      const endChildren = (endNode as Node).childNodes
      endNode = endChildren[endChildren.length - 1]
    }
    range.setEnd(endNode, endNode.length)

    this.selectMarks(marks)

  }

  // eslint-disable-next-line no-unused-vars
  protected xOnMouseUpSelection(e: Event, charRange: [number, number]) {

    e.stopPropagation()

  }

  // eslint-disable-next-line no-unused-vars
  protected xOnMouseUpMetaString(e: Event, charRange: [number, number], metaStringText: string) {

    e.stopPropagation()

  }

  protected xOnMouseUpOther(e: Event) {

    e.stopPropagation()

    this.deselect()

  }

}


class WordSelectionController extends TextSelectionController {

  // WordSelectionController only differs from TextSelectionController
  //  in how it handles mouse in text; it attempts to expand or contract
  //  the selection to the nearest whitespace boundaries.
  protected xOnMouseUpText(e: Event, charRange: [number, number]) {

    e.stopPropagation()

    // Grab selection and determine beg/end char
    const selection = window.getSelection()
    if (!selection) return

    if (!(selection.anchorNode instanceof Text)
     || !(selection.focusNode instanceof Text)) {
      console.error(
        'Window selection behaving strangely; anchor or focus not a Text.'
      )
      this.deselect()
      return
    }

    let isReversed = false
    const range = document.createRange()
    range.setStart(selection.anchorNode, selection.anchorOffset)
    range.setEnd(selection.focusNode, selection.focusOffset)

    if (range.compareBoundaryPoints(Range.START_TO_END, range) !== 1) {
      // If end < start, reverse them
      isReversed = true
      range.setStart(selection.focusNode, selection.focusOffset)
      range.setEnd(selection.anchorNode, selection.anchorOffset)
    }

    if (range.toString().length === 0) {
      if ((selection.focusNode as Text).length > selection.focusOffset) {
        range.setEnd(range.endContainer, range.endOffset + 1)
      }
    }

    const text = range.toString()

    // If nothing but whitespace, deselect
    if (!/\S/.test(text)) {
      this.deselect()
      return
    }

    // Contract from whitespace, or expand to first char/digit
    // This should work as long as there are no spans in the middle of whitespace.
    let begChar: number
    let endChar: number
    if (/\s/.test(text[0])) {
      // If first char is whitespace, contract
      begChar = text.search(/(\S)/) + range.startOffset
    } else {
      // Otherwise, expand selection back to first char/digit or node beginning
      const preRange = document.createRange()
      preRange.setStart(range.startContainer, 0)
      preRange.setEnd(range.startContainer, range.startOffset)
      // Note the +1 works for the -1 case as well!
      begChar = preRange.toString().search(/([^\w\d])[\w\d]*$/) + 1
    }

    if (/\s/.test(text[text.length - 1])) {
      // If last char is whitespace, contract
      endChar = text.search(/(\S)\s*$/) + range.endOffset - text.length + 1
      if (endChar < 0) endChar = 0
    } else {
      // Expand selection up to last char/digit or node ending
      const postRange = document.createRange()
      postRange.setStart(range.endContainer, range.endOffset)
      postRange.setEnd(range.endContainer, (range.endContainer as Text).length)
      endChar = postRange.toString().search(/([^\w\d])/)
      if (endChar === -1) endChar = (range.endContainer as Text).length
      else endChar += range.endOffset
    }

    // Finalize range and set selection to range
    range.setStart(range.startContainer, begChar)
    range.setEnd(range.endContainer, endChar)
    selection.removeAllRanges()
    selection.addRange(range)

    // Find chars between doc start and range start; recalibrate beg/endChar
    const textLength = range.toString().length
    if (isReversed) {
      begChar += charRange[0]
      endChar = begChar + textLength
    } else {
      endChar += charRange[0]
      begChar = endChar - textLength
    }

    this.selectNew(begChar, endChar)

  }

}


type ClickState = 'empty' | 'clicked'

class TwoClickController extends TextSelectionController {

  // TwoClickController holds state, either on its first or second click
  private clickState: ClickState = 'empty'
  private firstClickChar: number | null = null
  // Also hold a metachar structure to indicate first click

  private onFirstClick() {

    if (!this.composable) return

    // Grab selection and determine beg char
    const selection = window.getSelection()
    if (!selection) return

    if (!(selection.anchorNode instanceof Text)) {
      console.error(
        'Window selection behaving strangely; anchor or focus not a Text.'
      )
      this.deselect()
      return
    }

    this.clickState = 'clicked'
    this.firstClickChar = this.composable.manuscript.nodeOffsetToChar(
      selection.anchorNode, selection.anchorOffset
    )
    this.composable?.manuscript.metaStringClickChar(this.firstClickChar)
    this.deselect()

  }

  private onSecondClick(textRange: [number, number]) {

    // Grab selection and determine end char
    const selection = window.getSelection()
    if (!selection) return

    if (this.composable == null || this.firstClickChar == null || !(selection.focusNode instanceof Text)) {
      console.error(
        'Window selection behaving strangely; anchor or focus not a Text.'
      )
      this.deselect()
      return
    }

    let isReversed = false
    const [anchorEl, anchorOffset] = this.composable.manuscript.charToElementOffset(
      this.firstClickChar
    )
    const range = document.createRange()
    range.setStart(anchorEl.firstChild as Node, anchorOffset)
    range.setEnd(selection.focusNode, selection.focusOffset)

    if (range.compareBoundaryPoints(Range.START_TO_END, range) !== 1) {
      // If end < start, reverse them
      isReversed = true
      range.setStart(selection.focusNode, selection.focusOffset)
      range.setEnd(anchorEl.firstChild as Node, anchorOffset)
    }

    if (range.toString().length === 0) {
      if ((selection.focusNode as Text).length > selection.focusOffset) {
        range.setEnd(range.endContainer, range.endOffset + 1)
      }
    }

    let begChar = range.startOffset
    let endChar = range.endOffset

    // Finalize range and set selection to range
    range.setStart(range.startContainer, begChar)
    range.setEnd(range.endContainer, endChar)
    selection.removeAllRanges()
    selection.addRange(range)

    // Find chars between doc start and range start; recalibrate beg/endChar
    const textLength = range.toString().length
    if (isReversed) {
      begChar += textRange[0]
      endChar = begChar + textLength
    } else {
      endChar += textRange[0]
      begChar = endChar - textLength
    }

    this.resetClick()

    this.selectNew(begChar, endChar)

  }

  private resetClick() {
    this.clickState = 'empty'
    this.firstClickChar = null
    // Unset metachar
    this.composable?.manuscript.removeMetaStrings('click-char')
  }


  protected xOnMouseUpText(e: Event, charRange: [number, number]) {

    // MAYBE TODO: Check here that selection range (not charRange!) is trivial,
    //  otherwise use normal selection

    e.stopPropagation()

    switch (this.clickState) {
      case 'empty':
        this.onFirstClick()
        break
      case 'clicked':
        this.onSecondClick(charRange)
        break
    }

  }

  protected xOnMouseUpMark(e: Event, charRange: [number, number], marks: IMark[]) {
    this.resetClick()
    super.xOnMouseUpMark(e, charRange, marks)
  }
  protected xOnMouseUpSelection(e: Event, charRange: [number, number]) {
    this.resetClick()
    super.xOnMouseUpSelection(e, charRange)
  }
  protected xOnMouseUpMetaString(e: Event, charRange: [number, number], metaStringText: string) {
    this.resetClick()
    super.xOnMouseUpMetaString(e, charRange, metaStringText)
  }
  protected xOnMouseUpOther(e: Event) {
    this.resetClick()
    super.xOnMouseUpOther(e)
  }

}


export {
  NullController,
  TextSelectionController,
  WordSelectionController,
  TwoClickController
}
