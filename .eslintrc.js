module.exports = {
  root: true,
  env: {
    mocha: true,
    node: true
  },
  globals: {
    globalThis: false,
    LeaderLine: false
  },
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@typescript-eslint/parser',
  },
  plugins: [
    '@typescript-eslint',
    'mocha',
  ],
  extends: [
    'plugin:@typescript-eslint/recommended',
    'plugin:vue/base',
    'eslint:recommended',
  ],
  rules: {
    '@typescript-eslint/ban-types': 0,
    '@typescript-eslint/no-empty-interface': 0,
    '@typescript-eslint/explicit-module-boundary-types': 0,
    '@typescript-eslint/no-explicit-any': 0,
    '@typescript-eslint/no-inferrable-types': [1, { ignoreParameters: true, ignoreProperties: true }],
    '@typescript-eslint/no-unused-vars': [1, { args: 'none' }],
    'max-len': [1, 120],
    'new-parens': [1, 'always'],
    'no-console': [1, { allow: [ 'warn', 'error' ]}],
    'no-useless-escape': 0,
    'quote-props': [1, 'consistent-as-needed' ],
    'quotes': [1, 'single', { avoidEscape: true, allowTemplateLiterals: true }],
    'semi': [1, 'never']
  }
}
