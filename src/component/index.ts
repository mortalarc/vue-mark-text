// Manually add IMarkedText as part of MarkedText's type
// Warning: could potentially mess us if Component<IMarkedText> != Component<{}> & IMarkedText
import { IMarkedText } from '@/IMarkedText'
import { default as VMarkedText } from './MarkedText.vue'
const MarkedText = VMarkedText as IMarkedText & typeof VMarkedText
export { MarkedText }
