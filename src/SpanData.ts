import { IController } from './Controller'
import { IMark } from './IMark'


enum TypeSpanData {
  Text, Mark, Selection, Meta
}

type SpanControllerCbk = (event: Event, spanData: SpanData, controller: IController) => void

class SpanData {

  public charRange: [number, number]
  public key: string
  public cssClasses: string[]
  public subjects: string[]
  // Be careful with state in onCbk; it might not clone correctly!
  public spanControllerEvents: { [eventName: string]: SpanControllerCbk }
  public text: string

  public constructor(
    charRange: [number, number],
    key: string,
    cssClasses: string[],
    subjects: string[],
    spanControllerEvents: { [eventName: string]: SpanControllerCbk },
    text: string
  ) {
    this.charRange = charRange
    this.key = key
    this.cssClasses = cssClasses
    this.subjects = subjects
    this.spanControllerEvents = spanControllerEvents
    this.text = text
  }

  public clone() {
    return new SpanData(
      this.charRange.slice() as [number, number],
      this.key.slice(),
      this.cssClasses.slice(),
      this.subjects.slice(),
      this.spanControllerEvents,
      this.text.slice()
    )
  }

  public equal(other: SpanData) {
    return (
      this.charRange[0] === other.charRange[0] &&
      this.charRange[1] === other.charRange[1] &&
      this.key === other.key &&
      this.cssClasses.length === other.cssClasses.length &&
      this.cssClasses.every((c, idx) => c === other.cssClasses[idx]) &&
      this.subjects.length === other.subjects.length &&
      this.subjects.every((c, idx) => c === other.subjects[idx]) &&
      this.text === other.text
    )
  }

}



interface SpanDataArgs {
  charRange: [number, number],
  spanText: string,
  marks?: IMark[]
}

function createSpanData(typeSpanData: TypeSpanData, args: SpanDataArgs) {
  switch (typeSpanData) {
  case TypeSpanData.Text:
    return TextSpanData(args.charRange, args.spanText)
  case TypeSpanData.Mark:
    return MarkSpanData(args.charRange, args.spanText, args.marks)
  case TypeSpanData.Selection:
    return SelectionSpanData(args.charRange, args.spanText)
  case TypeSpanData.Meta:
    return MetaStringSpanData(args.charRange, args.spanText)
  }
}

function TextSpanData(
  charRange: [number, number],
  spanText: string
) {
  return new SpanData(
    charRange,
    'text-' + charRange[0] + '-' + charRange[1],
    [ 'mt-textspan' ],
    [],
    {
      mouseup: (event, spanData, controller) => controller.onMouseUpText(event, spanData.charRange)
    },
    spanText
  )
}

function MarkSpanData(
  charRange: [number, number],
  spanText: string,
  marks?: IMark[]
) {
  if (!marks?.length) return TextSpanData(charRange, spanText)
  else {
    const subjects = Array.from(new Set(Array<string>().concat(...marks.map(mark => mark.subjects))))
    const cssClasses = subjects.map(subject => 'mt-subject-' + subject)
    // TODO: Use default class when no subject
    return new SpanData(
      charRange,
      'markrange-' + charRange[0] + '-' + charRange[1],
      cssClasses,
      subjects,
      {
        mouseup: (event, spanData, controller) => controller.onMouseUpMark(event, spanData.charRange, marks)
      },
      spanText
    )
  }
}

function SelectionSpanData(
  charRange: [number, number],
  spanText: string,
) {
  return new SpanData(
    charRange,
    'selection-' + charRange[0] + '-' + charRange[1],
    [ 'mt-selection' ],
    [],
    {
      mouseup: (event, spanData, controller) => controller.onMouseUpSelection(event, spanData.charRange)
    },
    spanText
  )
}

function MetaStringSpanData(
  charRange: [number, number],
  spanText: string
) {
  return new SpanData(
    charRange,
    'meta-char-' + spanText + '-' + charRange[0] + '-' + charRange[1],
    [ 'mt-meta-char' ],
    [],
    {
      mouseup: (event, spanData, controller) => controller.onMouseUpMetaString(event, spanData.charRange, spanText)
    },
    spanText
  )
}



export {
  SpanData,
  SpanControllerCbk,
  TypeSpanData, createSpanData,
  TextSpanData, MarkSpanData, SelectionSpanData, MetaStringSpanData
}
