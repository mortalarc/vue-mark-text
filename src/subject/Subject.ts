import chroma from 'chroma-js'

export type SubjectStyleType = 'text' | 'background' | 'border' | 'underline'


export interface ISubject {
  name: string
  color: string
  active: boolean
  style: any
}


export class Subject implements ISubject {

  public name: string
  public color: string
  private styleType: SubjectStyleType
  // TODO: add css subject to this class

  private xActive: boolean
  public get active() { return this.xActive }
  public set active(val) { this.xActive = val }

  public constructor(
    name: string, color: string,
    styleType: SubjectStyleType = 'border'
  ) {
    this.name = name
    this.color = color
    this.styleType = styleType
    this.active = true
  }

  public get style() {
    const style: any = { }
    if (this.styleType === 'background') {
      style['background-color'] = this.color
      // If contrast with black >= 4.5 (WCAG rec), use black; otherwise white
      if (chroma.contrast(this.color, '#000000') >= 4.5)
        style.color = '#000000'
      else
        style.color = '#ffffff'
    }
    if (this.styleType === 'text') style.color = this.color
    if (this.styleType === 'border') {
      style.border = 'dotted 0.1em ' + this.color
      style['border-radius'] = '5px'
    }
    if (this.styleType === 'underline') {
      style['border-bottom'] = 'solid 0.1em ' + this.color
    }
    return style
  }

}
