import chroma from 'chroma-js'
import { ISubjectComposable } from './IMarkedText'


export interface IColorModule {
  link(composable: ISubjectComposable): void
  unlink(): void
  subjectColor(subject: string): string
}


export class ManualColorModule implements IColorModule {

  private composable: ISubjectComposable | null
  private subjectColorMap: Map<string, string>
  private defaultColor: string

  public constructor(defaultColor: string = 'black') {
    this.composable = null
    this.subjectColorMap = new Map()
    this.defaultColor = defaultColor
  }

  public link(composable: ISubjectComposable): void {
    this.composable = composable
  }
  public unlink() {
    if (this.composable != null) {
      this.composable = null
    }
  }

  public subjectColor(subject: string): string {
    let color = this.subjectColorMap.get(subject)
    if (color == null) {
      color = this.defaultColor
    }
    return color
  }

  public setSubjectColor(subject: string, color: string) {
    this.subjectColorMap.set(subject, color)
    this.composable?.resetColors()
  }
  public unsetSubjectColor(subject: string) {
    this.subjectColorMap.delete(subject)
    this.composable?.resetColors()
  }

}


export class ColorScaleModule implements IColorModule {

  private composable: ISubjectComposable | null
  private subjectColorLightness: number
  private numSubjectColors: number
  private subjectColorMap: Map<string, string>
  private colorIdx: number
  private generatedColors: string[]

  public constructor(subjectColorLightness: number, numSubjectColors: number) {
    this.composable = null
    this.subjectColorLightness = subjectColorLightness
    this.numSubjectColors = numSubjectColors
    this.subjectColorMap = new Map()
    this.colorIdx = 0
    this.generatedColors = this.generateColors()
  }

  public link(composable: ISubjectComposable): void {
    this.composable = composable
  }
  public unlink() {
    if (this.composable != null) {
      this.composable = null
    }
  }

  public subjectColor(subject: string): string {
    let color = this.subjectColorMap.get(subject)
    if (color == null) {
      color = this.generatedColors[this.colorIdx]
      this.colorIdx = (this.colorIdx + 1) % this.numSubjectColors
      this.subjectColorMap.set(subject, color)
    }
    return color
  }

  public setNumSubjectColors(val: number) {
    this.numSubjectColors = val
    this.composable?.$forceUpdate()
  }
  public setSubjectColorLightness(val: number) {
    this.subjectColorLightness = val
    this.composable?.$forceUpdate()
  }

  private generateColors() {

    // Create a color scale with the desired brightness
    const spectrum = chroma.scale(
      [ 'ff0000', 'ffff00', '00ff00', '00ffff', '0000ff', 'ff00ff', 'ff0000' ].map(
        colorStr => chroma.hex(colorStr).hsl()
      ).map(
        hcl => chroma.hsl(hcl[0], hcl[1], hcl[2] * this.subjectColorLightness)
      )
    )

    const colors = spectrum.colors(this.numSubjectColors + 1).slice(0, this.numSubjectColors)

    // Mix it up a bit more & get different colors earlier.
    // Find a non-divisor to go to next color by about a 6th
    let diff = Math.max(Math.round(colors.length / 6.0), 1)
    while (diff > 1 && colors.length % diff === 0) diff -= 1
    if (diff === 1) return colors

    const newColors = colors.slice()
    let idx = 0
    for (const color of colors) {
      newColors[idx] = color
      idx += diff
      if (idx >= newColors.length) idx -= newColors.length
    }

    return newColors

  }

}
