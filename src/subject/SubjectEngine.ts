import 'leader-line'

import { IColorModule } from '@/ColorModule'
import { ISubjectComposable } from '@/IMarkedText'
import { ISubjectLedger } from './SubjectLedger'
import { Subject } from './Subject'


export interface ISubjectEngine {
  link(markedText: ISubjectComposable): void
  unlink(): void
  setLedger(newLedger: ISubjectLedger): void
  setColorModule(newColorModule: IColorModule): void
  addSubjectsFromText(): void
  addSubjects(subjects: Iterable<string>): void
  resetColors(): void
  lineConnectSubject(subjectName: string): void
  lineDisconnectSubject(subjectName: string): void
  toggleLines(subjectName: string): void
}


export class SubjectEngine implements ISubjectEngine {

  private markedText: ISubjectComposable | null
  private ledger: ISubjectLedger
  private colorModule: IColorModule
  private leaderLines: Map<string, LeaderLine[]>

  public constructor(ledger: ISubjectLedger, colorModule: IColorModule) {
    this.markedText = null
    this.ledger = ledger
    this.colorModule = colorModule
    this.leaderLines = new Map()
  }


  public link(markedText: ISubjectComposable) {
    this.markedText = markedText
    this.ledger.link(markedText)
    this.colorModule.link(markedText)
  }
  public unlink() {
    if (this.markedText != null) {
      this.colorModule.unlink()
      this.ledger.unlink()
      this.markedText = null
    }
  }

  public setLedger(newLedger: ISubjectLedger) {
    if (this.ledger !== newLedger) {
      this.ledger.unlink()
      this.ledger = newLedger
      if (this.markedText) this.ledger.link(this.markedText)
      this.addSubjectsFromText()
    }
  }

  public setColorModule(newColorModule: IColorModule) {
    if (this.colorModule !== newColorModule) {
      this.colorModule.unlink()
      this.colorModule = newColorModule
      if (this.markedText) this.colorModule.link(this.markedText)
      this.resetColors()
    }
  }


  public addSubjectsFromText() {

    // TODO: Make this cheaper, just see which subjects changed

    if (this.markedText == null) {
      throw Error('Link SubjectLedger with MarkedText before calling resetSubjects')
    }

    const subjectNames = new Set(
      Array<string>().concat(...this.markedText.marks.map(mark => mark.subjects))
    )
    for (const subjectName of subjectNames) {
      if (!this.ledger.glossary.get(subjectName)) {
        this.ledger.glossary.set(subjectName, new Subject(
          subjectName,
          this.colorModule.subjectColor(subjectName),
          this.markedText.subjectStyleType
        ))
      }
    }

    this.markedText.$forceUpdate()

  }

  public addSubjects(subjects: Iterable<string>) {

    if (this.markedText == null) {
      throw Error('Link SubjectLedger with MarkedText before calling addSubjects')
    }

    for (const subjectName of subjects) {
      if (!this.ledger.glossary.has(subjectName)) {
        this.ledger.glossary.set(subjectName, new Subject(
          subjectName,
          this.colorModule.subjectColor(subjectName),
          this.markedText.subjectStyleType
        ))
      }
    }

  }

  public resetColors() {
    for (const sub of this.ledger.glossary.values()) {
      sub.color = this.colorModule.subjectColor(sub.name)
    }
  }


  public lineConnectSubject(subjectName: string) {
    const subject = this.ledger.glossary.get(subjectName)
    if (subject != null) {
      const leaderLines = []
      const spans = this.subjectSpans(subjectName)
      for (let idx = 1; idx < spans.length; ++idx) {
        leaderLines.push(new LeaderLine(
          spans[idx - 1], spans[idx], {
            startPlug: 'square',
            endPlug: 'square',
            color: subject.color,
            path: 'magnet',
            size: 1
          }
        ))
      }
      this.leaderLines.set(subjectName, leaderLines)
    }
  }
  public lineDisconnectSubject(subjectName: string) {
    this.leaderLines.get(subjectName)?.forEach(leaderLine => { leaderLine.remove() })
    this.leaderLines.delete(subjectName)
  }
  public toggleLines(subjectName: string) {
    if (this.leaderLines.has(subjectName)) this.lineDisconnectSubject(subjectName)
    else this.lineConnectSubject(subjectName)
  }

  public subjectCSSClassName(subjectName: string) { return 'mt-subject-' + subjectName }

  public subjectSpans(subjectName: string): HTMLSpanElement[] {
    const spans = Array.from(
      this.markedText?.$el.getElementsByClassName(this.subjectCSSClassName(subjectName)) || []
    )
    return spans as HTMLSpanElement[]
  }

}
