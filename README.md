vue-mark-text
===

This is a Vue component for marking/highlighting text.

Features
---

- Highlight specific char ranges, not word search
- Multiple colors
- Batched text for quick replacement
- Event listeners, managed through a controller
- Overlapping marks

See [documentation](https://mortalarc.gitlab.io/vue-mark-text/)
 for example of use.
